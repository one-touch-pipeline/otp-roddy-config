<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration configurationType='project' name='otpAlignmentAndQCWorkflowsChipSeq-1.0'
               imports="otp-1.1">
    <configurationvalues>

        <!-- We do run FASTQC in OTP independently. -->
        <cvalue name="runFastQC" value="false" type="boolean"
                description="Run FastQc or not. Default: true"/>
        <!-- The default CO config says, use the old bwa-aln and bwa-sampe workflow and align on the convey. PanCancer is bwa-mem and no convey, though. -->
        <cvalue name='useCombinedAlignAndSampe' value='true' type="boolean"/>
        <cvalue name='runSlimWorkflow' value='true' type="boolean"/>
        <cvalue name='useAcceleratedHardware' value='false' type="boolean"
                description="Use the Convey or not. Default for official PCAWF: false"/>

        <!-- BWA Software. -->
        <cvalue name="BWA_VERSION" value="0.7.8" type="string"
                description="Never ever change this value, without adapting the old project XMLs."/>

        <!-- Reduce memory used for sample sort -->
        <cvalue name="SAMPESORT_MEMSIZE" value="500000000" type="integer"/>
    </configurationvalues>

    <!-- Filename definitions. Don't change these, unless you know what you do. -->
    <filenames package='de.dkfz.b080.co.files' filestagesbase='de.dkfz.b080.co.files.COFileStage'>
        <!-- Filenames are always stored in the pid's output folder -->
        <!-- Different variables can be used:
         - ${sourcefile}, use the name and the path of the file from which the new name is derived
         - ${sourcefileAtomic}, use the atomic name of which the file is derived
         - ${sourcefileAtomicPrefix,delimiter=".."}, use the atomic name's prefix (without file-ending like .txt/.paired.bam...
         of which the file is derived, set the delimiter option to define the delimiter default is "_"
         the delimiter has to be placed inside "" as this is used to find the delimiter!
         - ${sourcepath}, use the path in which the source file is stored
         - ${outputbasepath}, use the output path of the pid
         - ${[nameofdir]OutputDirectory}

             NOTICE: If you use options for a variable your are NOT allowed to use ","! It is used to recognize options.

             - ${pid}
         - ${sample}
         - ${run}
         - ${lane}
         - ${laneindex}
         - You can put in configuration values to do this use:
         ${cvalue,name=[name of the value],default=".."} where default is optional.
         - ${fileStageID} use the id String of the file's stage to build up the name.
        -->
        <!-- A filename can be derived from another file, use derivedFrom='shortClassName/longClassName'
         A filename can also be specified for a level, use fileStage='PID/SAMPLE/RUN/LANE/INDEXEDLANE', refer to BaseFile.FileStage
         A filename can be specified for all levels, the name is then build up with the ${fileStageID} value
         A filename can be created using the file's called method's name
         A filename can be created using the used tool's name
        -->


        <!-- ##################################### General ##################################### -->
        <!-- Coverage plot files. These rules are currently used by all workflows in the AlignmentAndQCWorkflows plugin. -->
        <filename class='GenomeCoveragePlotFile' derivedFrom='CoverageTextFile[2]'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${pid}_${sample[0]}_vs_${sample[1]}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows_coveragePlot.png'/>
        <filename class='GenomeCoveragePlotFile' derivedFrom='CoverageTextFile'
                  selectiontag="singlePlot"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${pid}_${sample}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows_coveragePlot.png'/>


        <!-- ##################################### WGS ######################################### -->
        <!-- Lane BAMs and associated files -->
        <filename class='BamIndexFile' derivedFrom='BamFile' pattern='${sourcefile}.bai'/>
        <filename class='FastqcFile' onMethod='LaneFile.calcFastqc'
                  pattern="${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${fastx_qcOutputDirectory}/${dataSet}_${sample}_${run}_${laneindex}_sequence_fastqc.zip"/>

        <filename class='BamFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/${sample}_${run}_${lane}_${cvalue,name="pairedBamSuffix"}'/>
        <filename class='FlagstatsFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${flagstatsOutputDirectory}/${sourcefileAtomic}_flagstats.txt'/>
        <filename class='TextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag="extendedFlagstats"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${flagstatsOutputDirectory}/${sourcefileAtomic}_extendedFlagstats.txt'/>
        <filename class='BamMetricsFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${sourcefileAtomic}.dupmark_metrics.txt'/>
        <filename class='ChromosomeDiffValueFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png_qcValues.txt'/>
        <filename class='ChromosomeDiffTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.txt'/>
        <filename class='ChromosomeDiffPlotFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png'/>
        <filename class='InsertSizesValueFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png_qcValues.txt'/>
        <filename class='InsertSizesTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsizes.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png'/>
        <filename class='QCSummaryFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${sourcefileAtomicPrefix,delimiter="_"}_${sourcefileProperty,type}_wroteQcSummary.txt'/>
        <filename class='CoverageTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag='genomeCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.DepthOfCoverage_Genome.txt'/>
        <filename class='CoverageTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag='readBinsCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows.txt'/>
        <filename class="TextFile" onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag="fingerprints"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${fingerprintsOutputDirectory}/${sourcefileAtomic}.fp'/>
        <filename class="TextFile" onMethod='LaneFileGroup.alignAndPairSlim' selectiontag="qcJson"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/qualitycontrol.json'/>

        <!-- Merged BAM and associated files -->
        <filename class='BamFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/${sample}_${pid}_${cvalue,name="defaultMergedBamSuffix"}'/>
        <filename class='FlagstatsFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_flagstats.txt'/>
        <filename class='TextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="extendedFlagstats"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_extendedFlagstats.txt'/>
        <filename class='BamMetricsFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${sourcefileAtomic}.dupmark_metrics.txt'/>
        <filename class='ChromosomeDiffValueFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png_qcValues.txt'/>
        <filename class='ChromosomeDiffTextFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.txt'/>
        <filename class='ChromosomeDiffPlotFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png'/>
        <filename class='InsertSizesValueFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png_qcValues.txt'/>
        <filename class='InsertSizesTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsizes.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png'/>
        <filename class='QCSummaryFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${sourcefileAtomicPrefix,delimiter="_"}_${sourcefileProperty,type}_wroteQcSummary.txt'/>
        <filename class='CoverageTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag='genomeCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.DepthOfCoverage_Genome.txt'/>
        <filename class='CoverageTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag='readBinsCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows.txt'/>
        <filename class="TextFile" onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="qcJson"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/qualitycontrol.json'/>
        <filename class='BamMetricsAlignmentSummaryFile' onMethod='BamFile.collectMetrics'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${metricsOutputDirectory}/${cvalue,name="COLLECT_METRICS_PREFIX"}.alignment_summary_metrics'/>
        <filename class="TextFile" onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="fingerprints"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${fingerprintsOutputDirectory}/${sourcefileAtomic}.fp'/>
    </filenames>

</configuration>
