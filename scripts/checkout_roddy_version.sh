#!/bin/bash

#
# Copyright 2011-2024 The OTP authors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
#

if [ ! "$2" ]; then
    echo "Not enough parameters. The command is:"
    echo "checkout_roddy_version.sh RODDY_VERSION RODDY_BASE_DIRECTORY"
    exit 1
fi

#preperation
set -e
module load jdk/8u172 groovy/2.4.15

set -vx
export RODDY_VERSION=$1
export RODDY_DIRECTORY=$2

#clone roddy
cd $RODDY_DIRECTORY
cd roddy
git clone --depth 1 --single-branch --branch "$RODDY_VERSION" https://github.com/TheRoddyWMS/Roddy.git "$RODDY_VERSION"

#prepare base plugins
cd "$RODDY_VERSION"
cd dist/plugins/

#1. core plugin with version
git clone --depth 1 --single-branch --branch "1.2.2-3" https://github.com/TheRoddyWMS/Roddy-Default-Plugin DefaultPlugin_1.2.2
git clone --depth 1 --single-branch --branch "1.2.1" https://github.com/TheRoddyWMS/Roddy-Base-Plugin PluginBase_1.2.1
git clone --depth 1 --single-branch --branch "1.3.0" https://github.com/TheRoddyWMS/COWorkflowsBasePlugin COWorkflowsBasePlugin_1.3.0-0

## core plugin without version
git clone --depth 1 --single-branch --branch "master" https://github.com/TheRoddyWMS/Roddy-Default-Plugin DefaultPlugin
git clone --depth 1 --single-branch --branch "master" https://github.com/TheRoddyWMS/Roddy-Base-Plugin PluginBase

#build roddy
cd ../..
./gradlew build
cd dist/bin
ln -s develop $RODDY_VERSION
ln -s develop current
cd ../..
