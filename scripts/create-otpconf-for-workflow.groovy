/*
 * Copyright 2011-2024 The OTP authors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import groovy.json.JsonGenerator
import groovy.json.JsonOutput
import groovy.transform.Field
import groovy.transform.Immutable
import groovy.transform.TupleConstructor
import groovy.util.slurpersupport.GPathResult

import java.nio.file.DirectoryStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

// input _______________________________________________________________________________________________________________

// path to the workflow config file (location-specific)
String pluginBaseDirectory = ""

// path starting from pluginDirectory
String pathToWorkflowConfigFile = "${pluginBaseDirectory}/SNVCallingWorkflow_VERSION/resources/configurationFiles/analysisSNVCalling.xml"

String pathToCOWorkflowConfigFileDirectory = "${pluginBaseDirectory}/COWorkflows_COVERSION/resources/configurationFiles/"
String pathToCOWorkflowConfigFile = "${pluginBaseDirectory}/COWorkflows_COVERSION/resources/configurationFiles/commonCOWorkflowsSettings.xml"

String WorkflowName = "SNVCallingWorkflow"

ResourceSetSize resourceSetSize = ResourceSetSize.xl
ResourceSetSize testResourceSetSize = ResourceSetSize.t

// mapping workflow names used in the config files -> name used in the Workflow domain class
Map<String, String> roddyOtpWorkflowNameMap = [
        "SNVCallingWorkflow"  : "Roddy SNV calling",
]
// mapping sequencing type names used in the config files -> name used in the SeqType domain class
Map<String, String> roddyOtpSeqTypeNameMap = [
        wgs : "WHOLE_GENOME",
        wes : "EXON",
]
// grouping of latest normal config and resource config
Map<String, String> resources = [
        ("SNVCallingWorkflow:1.2.166-1-wes"): "otpSNVCallingWorkflowWES-1.0",
        ("SNVCallingWorkflow:1.2.166-1-wgs"): "otpSNVCallingWorkflowWGS-1.0",
        ("SNVCallingWorkflow:1.2.166-3-wes"): "otpSNVCallingWorkflowWES-1.0",
        ("SNVCallingWorkflow:1.2.166-3-wgs"): "otpSNVCallingWorkflowWGS-1.0",
        ("SNVCallingWorkflow:1.2.166-5-wes"): "otpSNVCallingWorkflowWES-1.0",
        ("SNVCallingWorkflow:1.2.166-5-wgs"): "otpSNVCallingWorkflowWGS-1.0",
]

// grouping of SNV plugin version and COWorkflows plugin version
Map<String, String> coWorkflows = [
        ("1.2.166-1"): "1.2.3-1",
        ("1.2.166-3"): "1.2.3-1",
        ("1.2.166-5"): "1.2.3-1",
]

boolean singleCell = false

String latestVersion = '1.2.166-5'
String outputFileName = 'snv2'

// Please adapt also the default values.
// Please decide, if they are used only for the latest or for all versions

// script ______________________________________________________________________________________________________________

File ewc = new File("tmp/ewc/${outputFileName}")
ewc.mkdirs()
File defaultDir = new File("tmp/defaults/${outputFileName}")
defaultDir.mkdirs()


@Field
RoddyConfMigrationHelper r = new RoddyConfMigrationHelper(args)

Map<String, Path> namePathMap = r.getNamePathMap()

println "fetching selected configs"
Map<String, Path> selectedConfigs = namePathMap.findAll { Map.Entry<String, Path> np ->
    //filter for workflows
    roddyOtpWorkflowNameMap.keySet().any { rn -> np.key.startsWith(rn) }
}.findAll { Map.Entry<String, Path> np ->
    //filter for seqtype
    roddyOtpSeqTypeNameMap.keySet().any { rn -> np.key.substring(0, np.key.length() - 11).endsWith(rn) }
}.findAll { Map.Entry<String, Path> np ->
    //filter for used versions
    resources.keySet().any { rn -> np.key.startsWith(rn) }
}

println "\n\nfetching latest configs"
Set<String> latestConfigs = selectedConfigs.keySet().findAll {
    it.contains(latestVersion)
}

@Immutable
class NVS {
    String name
    String version
    String seqType
    String sequencingReadType
}

@TupleConstructor
class Config {
    NVS nvs
    Map json
    boolean latest
}

Map<String, List<NVS>> confMapCn = [:].withDefault { [] }
Map<String, List<NVS>> confMapFileNames = [:].withDefault { [] }
Map<String, List<NVS>> confMapResource = [:].withDefault { [] }
Map<String, List<NVS>> testConfMap = [:].withDefault { [] }

Map<String, Map<String, Map<String, String>>> defaultConfigs = [:].withDefault {
    [:].withDefault {
        [:].withDefault {
            [:]
        }
    }
}

println "\n\nread configs from file system"
List<Config> configs = selectedConfigs.collectMany { Map.Entry<String, Path> it ->
    List<String> confName = [it.key]
    boolean latest = latestConfigs.contains(it.key)

    List<String> key = it.key.split("-") as List
    key.remove(key.size() - 1) // queue
    if (resources[key.join("-")]) {
        confName.add(resources[key.join("-")])
    }

    String seqType = roddyOtpSeqTypeNameMap[key.remove(key.size() - 1)]
    List<String> nameVersion = key.join("-").split(":") as List
    String name = roddyOtpWorkflowNameMap[nameVersion[0]]
    String version = nameVersion[1]

    return ['PAIRED'].collect { String sequencingReadType ->
        Map<String, Path> defaultRoddyValues = [(WorkflowName): Paths.get(pathToWorkflowConfigFile.replace("VERSION", version))]
        defaultRoddyValues.put("commonCOWorkflowsSettings", Paths.get(pathToCOWorkflowConfigFile.replace("COVERSION", coWorkflows[version])))
        Map conf = r.conv(confName, resourceSetSize, testResourceSetSize, namePathMap, latest, defaultRoddyValues)
        NVS nvs = new NVS(name, version, seqType, sequencingReadType)
        defaultConfigs[name][version][seqType][sequencingReadType] = conf
        return new Config(nvs, conf, latest)
    }
}


Map<String, ?> extractCommonEntries(List<Map<String, ?>> configs, String combinedKey = "", String prefix = "") {
    int configCount = configs.size()
    Map<String, ?> map = [:]

    List<String> commonKeys = configs.collectMany {
        it.keySet()
    }.countBy {
        it
    }.findAll {
        it.value == configCount
    }*.key

    commonKeys.each { String key ->
        println "${prefix}- check ${key}"
        List<?> values = configs*.get(key)
        boolean allValuesAreMap = values.every {
            it instanceof Map
        }
        if (allValuesAreMap) {
            Map<String, ?> nestedMap = extractCommonEntries(values, "${combinedKey}.${key}", "    ${prefix}")
            //do not add empty maps
            if (nestedMap) {
                map[key] = nestedMap
            }
            //remove empty nested maps
            configs.each {
                if (it[key].isEmpty()) {
                    it.remove(key)
                }
            }
        } else if (values.unique(false).size() == 1) {
            println "${prefix}  - extract common value ${values[0]}"
            map[key] = values[0]
            configs*.remove(key)
        } else {
            println "${prefix}  - neither map nor equal"
        }
    }
    return map
}

println "save default"
File defaultFile = new File(defaultDir, "default.json")
defaultFile.text = JsonOutput.prettyPrint(r.createJson(defaultConfigs))

println "\n\nstart extracting seq-type independent parts"
//extract common parts per version
List<Config> configsWithCommonSeparate = configs.groupBy {
    [it.nvs.name, it.nvs.version]
}.collectMany { List<String, String> nameAndVersion, List<Config> configsPerNameAndVersion ->
    println "Check ${nameAndVersion.join(' ')}: ${configsPerNameAndVersion*.nvs*.seqType}"
    if (configsPerNameAndVersion.size() > 1) {
        Map<String, ?> extracted = extractCommonEntries(configsPerNameAndVersion*.json)
        if (extracted) {
            Config config = new Config(
                    new NVS(nameAndVersion[0], nameAndVersion[1], null, null),
                    extracted, configsPerNameAndVersion.first().latest)
            configsPerNameAndVersion.add(0, config)
        } else {
            println "- no common data extracted"
        }
    } else {
        println "- skip, since only one file"
    }
    return configsPerNameAndVersion
}


void addToJsonMapIfNotEmpty(Map<String, List<NVS>> jsonMap, Map checkMap, NVS nvs, Closure<Map> wrapMaps) {
    if (checkMap) {
        String json = r.createJson(wrapMaps(checkMap ?: [:]))
        jsonMap[json].add(nvs)
    }
}

println "split per type, group same configs over versions"

//split types & group versions
configsWithCommonSeparate.each {
    addToJsonMapIfNotEmpty(confMapCn, it.json.prod?.RODDY?.cvalues, it.nvs) {
        [RODDY: [cvalues: it]]
    }
    addToJsonMapIfNotEmpty(confMapResource, it.json.prod?.RODDY?.resources, it.nvs) {
        [RODDY: [resources: it]]
    }
    addToJsonMapIfNotEmpty(confMapFileNames, it.json.prod?.RODDY_FILENAMES, it.nvs) {
        [RODDY_FILENAMES: it]
    }
    if (it.latest) {
        addToJsonMapIfNotEmpty(testConfMap, it.json.test, it.nvs) {
            it
        }
    }
}


String createSql(String conf, String type, String wf, List<String> version, String seqType, String sequencingReadType, boolean singleCell, boolean test) {
    String name = [
            "Default",
            type,
            "values for",
            wf,
            test ? "" : version.join(", "),
            seqType ? seqType : '',
            sequencingReadType ? sequencingReadType : '',
            test ? " test" : "",
    ]*.trim().findAll().join(' ')

    String sql = """
    INSERT INTO external_workflow_config_fragment(id, version, date_created, last_updated, object_version, name, config_values)
        VALUES(nextval('hibernate_sequence'), 0, now(), now(), 0, '${name}',
${conf}
    )
    ON CONFLICT DO NOTHING;

    INSERT INTO external_workflow_config_selector(id, version, date_created, last_updated, name, priority, selector_type, external_workflow_config_fragment_id)
    VALUES(nextval('hibernate_sequence'), 0, now(), now(), '${name}', ${test ? 100 : seqType ? 22 : 6}, 'DEFAULT_VALUES', (
       SELECT id FROM external_workflow_config_fragment WHERE name = '${name}'))
    ON CONFLICT DO NOTHING;

    INSERT INTO external_workflow_config_selector_workflow (external_workflow_config_selector_workflows_id, workflow_id)
        SELECT (SELECT id FROM external_workflow_config_selector WHERE name = '${name}'), (SELECT id FROM workflow WHERE name = '${wf}')
    ON CONFLICT DO NOTHING;
"""
    if (!test) {
        version.each {
            sql += """
    INSERT INTO external_workflow_config_selector_workflow_version (external_workflow_config_selector_workflow_versions_id, workflow_version_id)
        SELECT (SELECT id FROM external_workflow_config_selector WHERE name = '${name}'), (SELECT id FROM workflow_version WHERE api_version_id = (SELECT id FROM workflow_api_version WHERE workflow_id = (SELECT id FROM workflow WHERE name = '${wf}')) AND workflow_version.workflow_version = '${it}')
    ON CONFLICT DO NOTHING;
"""
        }
    }
    if (seqType) {
        sql += """
    INSERT INTO external_workflow_config_selector_seq_type (external_workflow_config_selector_seq_types_id, seq_type_id)
        SELECT (SELECT id FROM external_workflow_config_selector WHERE name = '${name}'), (SELECT id FROM seq_type WHERE name = '${seqType}' AND single_cell = ${singleCell} AND library_layout = '${sequencingReadType}')
    ON CONFLICT DO NOTHING;
"""
    }
    return sql
}


Closure<List<File>> createSql = { Map<String, List<NVS>> config, String key, boolean test ->
    List<File> files = []
    config.each { Map.Entry<String, List<NVS>> entry ->
        while (entry.value) {
            NVS element = entry.value.pop()
            List<NVS> withSameSeqType = entry.value.findAll { it.seqType == element.seqType && it.sequencingReadType == element.sequencingReadType }
            List<String> versions = ([element] + withSameSeqType).collect { it.version }.sort()

            String fileName = [
                    "ewc-roddy-${outputFileName}",
                    key,
                    element.seqType ? element.seqType.replaceAll(' ', '') : '',
                    element.sequencingReadType ? element.sequencingReadType.replaceAll(' ', '') : '',
                    test ? 'all-version' : versions.join('+'),
            ].findAll().join('-') + ".sql"
            File file = new File(ewc, fileName)
            file.text = createSql(r.formatJsonForSql(entry.key), key, element.name, versions, element.seqType, element.sequencingReadType, singleCell, test)
            entry.value -= withSameSeqType
            files << fileName
        }
    }
    return files
}

Closure<Void> createFileList = { List<File> sqlFiles ->
    println "files += ["
    println sqlFiles.collect {
        "ewcDir.resolve(\"${it}\"),"
    }.join('\n')
    println "]"
}

println "create sql files"
List<File> sqlFiles = [
        cvalue   : confMapCn,
        resources: confMapResource,
        filenames: confMapFileNames,
].collectMany { String type, Map<String, List<NVS>> map ->
    createSql(map, type, false)
}

createSql(testConfMap, "test-resource", true)

println "create include list"
createFileList(sqlFiles)

// common ______________________________________________________________________________________________________________

class RoddyConfMigrationHelper {
    Path pwd

    RoddyConfMigrationHelper(String[] args) {
        pwd = args.size() == 1 ?
                Paths.get(args[0]) :
                new File(".").toPath()
    }

    // create map: configuration name -> file name
    Map<String, Path> getNamePathMap(List<Path> additionalPaths = []) {
        Map<String, Path> namePathMap = [:]
        ([pwd, pwd.resolve("resource-templates/lsf")] + additionalPaths).each {
            DirectoryStream<Path> stream = Files.newDirectoryStream(it, "*.xml")
            for (Path entry : stream) {
                GPathResult configuration = new XmlSlurper().parse(entry.newReader())
                String name = configuration.@name
                namePathMap.put(name, entry)
            }
        }
        return namePathMap
    }

    @Immutable(knownImmutables = ['value'])
    static class CV {
        Object value
        String type
    }

    @Immutable
    class Resource {
        String value
        String basepath
        String memory
        Integer cores
        Integer nodes
        String walltime
    }

    @Immutable
    class ResourceSet {
        ResourceSetSize size
        String memory
        Integer cores
        Integer nodes
        String walltime
    }

    /**
     * this method is copied from roddy (de.dkfz.roddy.config.ToolEntry)
     */
    ResourceSet getResourceSet(ResourceSetSize key, List<ResourceSet> resourceSets) {
        int size = key.ordinal()

        if (resourceSets.size() == 1) { // Only one set exists.
            return resourceSets.get(0)
        }

        if (resourceSets.size() == 0)
            return new EmptyResourceSet()

        ResourceSet first = resourceSets.get(0);
        ResourceSet last = resourceSets.get(resourceSets.size() - 1);
        if (size <= first.getSize().ordinal()) {
            // The given key is smaller than the available keys. Return the first set.
            return first
        }
        if (size >= last.getSize().ordinal()) {
            // The given key is larger than the available keys. Return the last set.
            return last;
        }
        for (ResourceSet resourceSet : resourceSets) {  // Select the appropriate set
            if (resourceSet.getSize() == key)
                return resourceSet
        }
        //Still no set, take the largest set, which comes after the given ordinal.
        for (ResourceSet resourceSet : resourceSets) {
            if (resourceSet.getSize().ordinal() > size)
                return resourceSet
        }

        return new EmptyResourceSet()
    }

    class EmptyResourceSet {}

    private void convert(List<String> confNames, ResourceSetSize resourceSetSize, ResourceSetSize testResourceSetSize, Map<String, Path> namePathMap, Map<String, CV> cvMap, Map<String, Resource> resourcesMap, Map<String, Resource> testResourcesMap, List<Map> filenamesList) {

        confNames.each { String confName ->
            if (!confName) {
                return
            }
            Path path = namePathMap.get(confName)

            GPathResult configuration = new XmlSlurper().parse(path.newReader())
            List<String> imports = (configuration.@imports as String).split(",")

            convert(imports, resourceSetSize, testResourceSetSize, namePathMap, cvMap, resourcesMap, testResourcesMap, filenamesList)

            configuration.configurationvalues.children().each { cv ->
                if (cv.name() == "configurationValueBundle") {
                    cv.children().each { cv2 ->
                        addCv(cv2, cvMap)
                    }
                } else {
                    addCv(cv, cvMap)
                }
            }

            configuration.processingTools.children().each { tool ->
                List<ResourceSet> availableResourceSets = tool.resourcesets.children().collect { rset ->
                    new ResourceSet(ResourceSetSize.valueOf(rset.@size as String),
                            (rset.@memory as String) ?: (null as String),
                            rset.@cores as String ? Integer.parseInt(rset.@cores as String) : (null as Integer),
                            rset.@nodes as String ? Integer.parseInt(rset.@nodes as String) : (null as Integer),
                            (rset.@walltime as String) ?: (null as String)
                    )
                }.sort { a, b -> a.size.compareTo(b.size) }

                if (availableResourceSets.empty) {
                    return
                }

                ResourceSet res = getResourceSet(resourceSetSize, availableResourceSets)
                resourcesMap.put(tool.@name as String, new Resource(tool.@value as String, tool.@basepath as String, res.memory, res.cores, res.nodes, res.walltime))

                ResourceSet testRes = getResourceSet(testResourceSetSize, availableResourceSets)
                testResourcesMap.put(tool.@name as String,
                        new Resource(
                                tool.@value as String,
                                tool.@basepath as String,
                                (res.memory != testRes.memory) ? testRes.memory : null,
                                (res.cores != testRes.cores) ? testRes.cores : null,
                                (res.nodes != testRes.nodes) ? testRes.nodes : null,
                                (res.walltime != testRes.walltime) ? testRes.walltime : null,
                        )
                )
            }

            configuration.filenames.children().each { filename ->
                filenamesList.add(["class"          : filename.@class as String, pattern: filename.@pattern as String,
                                   selectiontag     : filename.@selectiontag as String ?: null,
                                   derivedFrom      : filename.@derivedFrom as String ?: null,
                                   fileStage        : filename.@fileStage as String ?: null,
                                   onMethod         : filename.@onMethod as String ?: null,
                                   onScriptParameter: filename.@onScriptParameter as String ?: null,
                                   onTool           : filename.@onTool as String ?: null])
            }
        }
    }

    private void addCv(cv, Map<String, CV> cvMap) {
        String type = (cv.@type as String) ?: null
        cvMap.put(cv.@name as String, new CV(type == "integer" ? Integer.parseInt(cv.@value as String) : (cv.@value as String), type))
    }

    Map conv(List<String> defaultConfName, ResourceSetSize resourceSetSize, ResourceSetSize testResourceSetSize, Map<String, Path> namePathMap, boolean latest, Map roddyDefaults) {
        Map<String, CV> cvaluesMap = [:]
        Map<String, Resource> resourcesMap = [:]
        Map<String, Resource> testResourcesMap = [:]
        List<Map> filenamesList = []

        // first get the workflow config file
        convert([roddyDefaults.keySet()[0]], resourceSetSize, testResourceSetSize, roddyDefaults, cvaluesMap, resourcesMap, testResourcesMap, filenamesList)
        convert(defaultConfName, resourceSetSize, testResourceSetSize, namePathMap, cvaluesMap, resourcesMap, testResourcesMap, filenamesList)

        // setting default values of the workflow
        if (latest || true) {
            // no default values
        }

        return [
                prod: [
                        RODDY          : [
                                cvalues  : cvaluesMap ?: null,
                                resources: resourcesMap ?: null,
                        ],
                        RODDY_FILENAMES: [
                                filenames: filenamesList ?: null,
                        ],
                ],
                test: [
                        RODDY: [
                                resources: testResourcesMap ?: null,
                        ],
                ],
        ]
    }

    private JsonGenerator generator = new JsonGenerator.Options()
            .excludeNulls()
            .build()

    String createJson(Object o) {
        return generator.toJson(o)
    }

    String formatJsonForSql(String json) {
        return JsonOutput.prettyPrint(json).split("\n").collect {
            "'${it.replaceAll(/\$\{pid\}/, /\$\{p'||'id\}/)}'"
        }.join(" ||\n")
    }
}

enum ResourceSetSize {
    t, xs, s, m, l, xl;
}
