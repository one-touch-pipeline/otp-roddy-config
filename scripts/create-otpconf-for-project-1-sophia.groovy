/*
 * Copyright 2011-2024 The OTP authors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import de.dkfz.tbi.otp.dataprocessing.ConfigPerProjectAndSeqType
import de.dkfz.tbi.otp.dataprocessing.Pipeline
import de.dkfz.tbi.otp.ngsdata.SeqType
import de.dkfz.tbi.otp.project.Project
import de.dkfz.tbi.otp.utils.CollectionUtils

/**
 * run this script in the OTP console
 *
 * input:
 *  - pipeline names -> workflow names
 *  - seq types
 *
 *  place output of this script in the next script
 */

LinkedHashMap<Pipeline.Name, String> pipelines = [
        (Pipeline.Name.RODDY_SOPHIA): "Roddy Sophia (structural variation calling)",
]

List<String> seqTypes = [
        "WHOLE_GENOME", "EXON"
]

// _____________________________________________________________________________________________________________________________________________________________

Map<Pipeline, String> pp = pipelines.collectEntries {
    Pipeline pipeline = CollectionUtils.exactlyOneElement(Pipeline.findAllByName(it.key))
    assert pipeline.usesRoddy()
    [(pipeline): it.value]
}

ConfigPerProjectAndSeqType getConfiguredWorkflowVersion(Project project, SeqType seqType, Pipeline pipeline) {
    ConfigPerProjectAndSeqType.createCriteria().get {
        eq("project", project)
        eq("seqType", seqType)
        eq("pipeline", pipeline)
        isNull("obsoleteDate")
        isNull("individual")
    } as ConfigPerProjectAndSeqType
}

StringBuffer output = new StringBuffer()
output << "\n"
Project.list().sort { it.name }.each { Project project ->
    pp.each { Pipeline pipeline, String workflow ->
        return pipeline.seqTypes.each { SeqType seqType ->
            if (seqType.name in seqTypes) {
                ConfigPerProjectAndSeqType conf = getConfiguredWorkflowVersion(project, seqType, pipeline)
                if (conf) {
                    output << "new Input('${project.name}', '${seqType.name}', '${seqType.libraryLayout}', ${seqType.singleCell}, '${workflow}', '${conf.programVersion.split(":")[1]}', '${conf.configFilePath}', '${conf.nameUsedInConfig}'),\n"
                }
            }
        }
    }
}

println output

[]
