/*
 * Copyright 2011-2024 The OTP authors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import groovy.json.JsonGenerator
import groovy.transform.Immutable
import groovy.transform.ToString
import groovy.transform.TupleConstructor
import groovy.util.slurpersupport.GPathResult

import java.nio.file.DirectoryStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * run this script on the command line with groovy 2.5+
 *
 */

// input _______________________________________________________________________________________________________________

List<Input> inputList = [
// copy input here

]

ResourceSetSize resourceSetSize = ResourceSetSize.xl

String outputFileName = 'snv2'

// path to the workflow config file (location-specific)
String pluginBaseDirectory = ""
// path starting from pluginDirectory
String pathToWorkflowConfigFile = "${pluginBaseDirectory}/SNVCallingWorkflow_VERSION/resources/configurationFiles/analysisSNVCalling.xml"
String pathToCOWorkflowConfigFileDirectory = "${pluginBaseDirectory}/COWorkflows_COVERSION/resources/configurationFiles/"
String pathToCOWorkflowConfigFile = "${pluginBaseDirectory}/COWorkflows_COVERSION/resources/configurationFiles/commonCOWorkflowsSettings.xml"

String WorkflowName = "SNVCallingWorkflow"

// grouping of SNV plugin version and COWorkflows plugin version
Map<String, String> coWorkflows = [
        ("1.2.166-1"): "1.2.3-1",
        ("1.2.166-3"): "1.2.3-1",
        ("1.2.166-5"): "1.2.3-1",
]

// script ______________________________________________________________________________________________________________

@ToString(excludes = ['configFilePath', 'nameUsedInConfig'])
@Immutable
class Input {
    String project
    String seqTypeName
    String seqTypeReadType
    boolean seqTypeSingleCell
    String workflow
    String version
    String configFilePath
    String nameUsedInConfig
}

@ToString
@TupleConstructor
class ParsedInput {
    Input input
    Map projectConfig
    Map defaultConfig
}

@ToString
@Immutable
class Group {
    String workflowName
    String version
    String seqTypeName
    String seqTypeReadType
    boolean seqTypeSingleCell
}

RoddyConfMigrationHelper r = new RoddyConfMigrationHelper(args)
List<String> header = [
        "import de.dkfz.tbi.otp.ngsdata.SeqType",
        "import de.dkfz.tbi.otp.project.Project",
        "import de.dkfz.tbi.otp.utils.CollectionUtils",
        "import de.dkfz.tbi.otp.workflowExecution.*",
        "",
        "ConfigSelectorService configSelectorService = ctx.configSelectorService",
        "CreateCommand createCommand",
        "Workflow workflow",
]

new File("tmp/projects").mkdirs()
new File('tmp/scripts').mkdirs()

File defaultJson = new File("tmp/defaults/${outputFileName}/default.json")
assert defaultJson.exists()

println "\nRead default "
Map defaultConfigs = new groovy.json.JsonSlurper().parseText(defaultJson.text)

Map<String, List<Input>> configsToCreate = [:].withDefault { [] }
List<ParsedInput> parsedInputList = []

println "\nRead ${inputList.size()} project configs"
inputList.eachWithIndex { Input input, int i ->
    println "- read ${input}"
    Map<String, Path> namePathMap = r.getNamePathMap([Paths.get(input.configFilePath).parent])

    Map<String, Path> defaultRoddyValues = [(WorkflowName): Paths.get(pathToWorkflowConfigFile.replace("VERSION", input.version))]
    defaultRoddyValues.put("commonCOWorkflowsSettings", Paths.get(pathToCOWorkflowConfigFile.replace("COVERSION", coWorkflows[input.version])))
    Map result = r.conv([input.nameUsedInConfig], resourceSetSize, resourceSetSize, namePathMap, defaultRoddyValues)
    Map projectConfig = [RODDY: [cvalues: result.prod.RODDY.cvalues]]

    Map defaultConfig = defaultConfigs[input.workflow][input.version][input.seqTypeName][input.seqTypeReadType]['prod']
    parsedInputList << new ParsedInput(input, projectConfig, defaultConfig,)
}

println "\nExtract updates"
parsedInputList.eachWithIndex { ParsedInput parsedInput, int i ->
    println "- extract updates for ${parsedInput.input.project} ${parsedInput.input.seqTypeName}"

    Map updates = extractUpdatesConfig(parsedInput.projectConfig, parsedInput.defaultConfig, "", "  ")
    if (updates) {
        String configForProjectSeqType = r.createJson(updates)
        println "  --> cleaned project config: ${configForProjectSeqType}"
        configsToCreate[configForProjectSeqType].add(parsedInput.input)
    } else {
        println "  --> empty project config"
    }
}

println "\nGroup ${configsToCreate.size()} different configs"
List<String> fragmentsToCreate = configsToCreate.collectMany { String json, List<Input> inputsToGroup ->
    inputsToGroup.groupBy {
        new Group(it.workflow, it.version, it.seqTypeName, it.seqTypeReadType, it.seqTypeSingleCell)
    }.collect { Group group, List<Input> inputs ->
        createScript(json, group, inputs)
    }
}

println "\nCreate ${fragmentsToCreate.size()} fragments"
File script = new File("tmp/scripts/create-otpconf-for-project-3.groovy")
println "create ${script}"
script.text = [
        header.join('\n'),
        "",
        fragmentsToCreate.join('\n'),
].join('\n\n')


println "\nfinish"

Map extractUpdatesConfig(Map projectConfig, Map defaultConfig, String combinedKey = "", String prefix = "") {
    Map<String, ?> map = [:]

    projectConfig.each { String key, Object value ->
        println "${prefix}- check ${key}"

        Object valueDefault = defaultConfig[key]

        if (value instanceof Map && valueDefault instanceof Map) {
            Map nestedMap = extractUpdatesConfig(value, valueDefault, "${combinedKey}.${key}", "    ${prefix}")
            //do not add empty maps
            if (nestedMap) {
                map[key] = nestedMap
            }
        } else if (value instanceof Map && valueDefault == null) {
            map[key] = value
        } else if (value instanceof RoddyConfMigrationHelper.CV) {
            RoddyConfMigrationHelper.CV cv = (RoddyConfMigrationHelper.CV) value
            if (valueDefault instanceof Map) {
                if (cv.value == valueDefault.value) {
                    println "${prefix}  - skip, since values are equal: ${cv.value}"
                } else {
                    map[key] = cv
                    println "${prefix}  - keep value, since not equal: "
                    println "${prefix}    - project: ${cv.value}"
                    println "${prefix}    - default: ${valueDefault.value}"
                }
            } else if (valueDefault == null) {
                map[key] = cv
                println "${prefix}  - keep value, since not in default config: ${cv.value} "
            } else {
                throw new RuntimeException("unexpected class for default config: ${valueDefault.getClass()}: ${valueDefault} ")
            }
        } else {
            throw new RuntimeException("unexpected class for project config: ${value.getClass()} & ${valueDefault.getClass()}")
        }
    }
    return map
}

String createScript(String configForProjectSeqType, Group group, List<Input> inputs) {
    String name = [
            group.workflowName,
            group.version,
            group.seqTypeName,
            group.seqTypeReadType,
            group.seqTypeSingleCell ? "SC" : "bulk",
            "for",
            inputs.size() < 5 ? inputs*.project.join(' & ') : inputs.first().project + ' and others',
    ].join(' ')
    String projects = inputs*.project.collect {
        "                CollectionUtils.exactlyOneElement(Project.findAllByName('${it}')),\n"
    }.join('')

    return """
println "${name}"
workflow = CollectionUtils.exactlyOneElement(Workflow.findAllByName('${group.workflowName}'))
createCommand = new CreateCommand(
        workflows: [workflow],
        workflowVersions: [CollectionUtils.exactlyOneElement(WorkflowVersion.findAllByApiVersionAndWorkflowVersion(CollectionUtils.exactlyOneElement(WorkflowApiVersion.findAllByWorkflow(workflow)), '${group.version}'))],
        projects: [\n${projects}        ],
        seqTypes: [SeqType.findAllByNameAndLibraryLayoutAndSingleCell('${group.seqTypeName}', '${group.seqTypeReadType}', ${group.seqTypeSingleCell})],
        referenceGenomes: [],
        libraryPreparationKits: [],
        selectorName: '${name}',
        type: SelectorType.GENERIC,
        value: '''${configForProjectSeqType.replaceAll("\\\\", "\\\\\\\\")}''',
)
configSelectorService.create(createCommand)

"""
}

// common ______________________________________________________________________________________________________________

class RoddyConfMigrationHelper {
    Path pwd

    RoddyConfMigrationHelper(String[] args) {
        pwd = args.size() == 1 ?
                Paths.get(args[0]) :
                new File(".").toPath()
    }

    // create map: configuration name -> file name
    Map<String, Path> getNamePathMap(List<Path> additionalPaths = []) {
        Map<String, Path> namePathMap = [:]

        ([pwd, pwd.resolve("resource-templates/lsf")] + additionalPaths).each {
            DirectoryStream<Path> stream = Files.newDirectoryStream(it, "*.xml")
            for (Path entry : stream) {
                GPathResult configuration = new XmlSlurper().parse(entry.newReader())
                String name = configuration.@name
                namePathMap.put(name, entry)
            }
        }
        return namePathMap
    }

    @Immutable(knownImmutables = ['value'])
    static class CV {
        Object value
        String type
    }

    @Immutable
    class Resource {
        String value
        String basepath
        String memory
        Integer cores
        Integer nodes
        String walltime
    }

    @Immutable
    class ResourceSet {
        ResourceSetSize size
        String memory
        Integer cores
        Integer nodes
        String walltime
    }

    /**
     * this method is copied from roddy (de.dkfz.roddy.config.ToolEntry)
     */
    ResourceSet getResourceSet(ResourceSetSize key, List<ResourceSet> resourceSets) {
        int size = key.ordinal()

        if (resourceSets.size() == 1) { // Only one set exists.
            return resourceSets.get(0)
        }

        if (resourceSets.size() == 0)
            return new EmptyResourceSet()

        ResourceSet first = resourceSets.get(0);
        ResourceSet last = resourceSets.get(resourceSets.size() - 1);
        if (size <= first.getSize().ordinal()) {
            // The given key is smaller than the available keys. Return the first set.
            return first
        }
        if (size >= last.getSize().ordinal()) {
            // The given key is larger than the available keys. Return the last set.
            return last;
        }
        for (ResourceSet resourceSet : resourceSets) {  // Select the appropriate set
            if (resourceSet.getSize() == key)
                return resourceSet
        }
        //Still no set, take the largest set, which comes after the given ordinal.
        for (ResourceSet resourceSet : resourceSets) {
            if (resourceSet.getSize().ordinal() > size)
                return resourceSet
        }

        return new EmptyResourceSet()
    }

    class EmptyResourceSet {}

    private void convert(List<String> confNames, ResourceSetSize resourceSetSize, ResourceSetSize testResourceSetSize, Map<String, Path> namePathMap,
                         Map<String, CV> cvMap, Map<String, Resource> resourcesMap, Map<String, Resource> testResourcesMap, List<Map> filenamesList,
                         boolean dontIncludeDependencies = false, List<String> dependencies = []
    ) {
        confNames.each { String confName ->
            confName = confName.trim()
            if (!confName) {
                return
            }
            Path path = namePathMap.get(confName)
            GPathResult configuration = new XmlSlurper().parse(path.newReader())
            List<String> imports = (configuration.@imports as String).split(",")
            println "    - ${confName} --> ${imports}"

            if (dontIncludeDependencies) {
                dependencies.addAll(imports)
            } else {
                convert(imports, resourceSetSize, testResourceSetSize, namePathMap, cvMap, resourcesMap, testResourcesMap, filenamesList, dontIncludeDependencies, dependencies)
            }

            [configuration.configurationvalues, configuration.subconfigurations.configuration.configurationvalues].each {
                it.children().each { cv ->
                    if (cv.name() == "configurationValueBundle") {
                        cv.children().each { cv2 ->
                            addCv(cv2, cvMap)
                        }
                    } else {
                        addCv(cv, cvMap)
                    }
                }
            }

            configuration.processingTools.children().each { tool ->
                List<ResourceSet> availableResourceSets = tool.resourcesets.children().collect { rset ->
                    new ResourceSet(ResourceSetSize.valueOf(rset.@size as String),
                            (rset.@memory as String) ?: (null as String),
                            rset.@cores as String ? Integer.parseInt(rset.@cores as String) : (null as Integer),
                            rset.@nodes as String ? Integer.parseInt(rset.@nodes as String) : (null as Integer),
                            (rset.@walltime as String) ?: (null as String)
                    )
                }.sort { a, b -> a.size.compareTo(b.size) }

                if (availableResourceSets.empty) {
                    return
                }

                ResourceSet res = getResourceSet(resourceSetSize, availableResourceSets)
                resourcesMap.put(tool.@name as String, new Resource(tool.@value as String, tool.@basepath as String, res.memory, res.cores, res.nodes, res.walltime))

                ResourceSet testRes = getResourceSet(testResourceSetSize, availableResourceSets)
                testResourcesMap.put(tool.@name as String,
                        new Resource(
                                tool.@value as String,
                                tool.@basepath as String,
                                (res.memory != testRes.memory) ? testRes.memory : null,
                                (res.cores != testRes.cores) ? testRes.cores : null,
                                (res.nodes != testRes.nodes) ? testRes.nodes : null,
                                (res.walltime != testRes.walltime) ? testRes.walltime : null,
                        )
                )
            }

            configuration.filenames.children().each { filename ->
                filenamesList.add(["class"          : filename.@class as String, pattern: filename.@pattern as String,
                                   selectiontag     : filename.@selectiontag as String ?: null,
                                   derivedFrom      : filename.@derivedFrom as String ?: null,
                                   fileStage        : filename.@fileStage as String ?: null,
                                   onMethod         : filename.@onMethod as String ?: null,
                                   onScriptParameter: filename.@onScriptParameter as String ?: null,
                                   onTool           : filename.@onTool as String ?: null])
            }
        }
    }

    private void addCv(cv, Map<String, CV> cvMap) {
        String type = (cv.@type as String) ?: null
        cvMap.put(cv.@name as String, new CV(type == "integer" ? Integer.parseInt(cv.@value as String) : (cv.@value as String), type))
    }

    Map conv(List<String> defaultConfName, ResourceSetSize resourceSetSize, ResourceSetSize testResourceSetSize, Map<String, Path> namePathMap, Map roddyDefaults) {
        println "  - ${defaultConfName}"

        Map<String, CV> cvaluesMap = [:]
        Map<String, Resource> resourcesMap = [:]
        Map<String, Resource> testResourcesMap = [:]
        List<Map> filenamesList = []

        // first get the workflow config file
        convert([roddyDefaults.keySet()[0]], resourceSetSize, testResourceSetSize, roddyDefaults, cvaluesMap, resourcesMap, testResourcesMap, filenamesList)
        convert(defaultConfName, resourceSetSize, testResourceSetSize, namePathMap, cvaluesMap, resourcesMap, testResourcesMap, filenamesList,)

        return [
                prod: [
                        RODDY          : [
                                cvalues  : cvaluesMap ?: null,
                                resources: resourcesMap ?: null,
                        ],
                        RODDY_FILENAMES: [
                                filenames: filenamesList ?: null,
                        ],
                ],
        ]
    }

    private JsonGenerator generator = new JsonGenerator.Options()
            .excludeNulls()
            .build()

    String createJson(Object o) {
        return generator.toJson(o)
    }
}

enum ResourceSetSize {
    t, xs, s, m, l, xl;
}
