#!/bin/bash

#
# Copyright 2011-2024 The OTP authors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#



##########################################################################
# This script is part of the otp-roddy-config repository.                #
#                                                                        #
# Do not change it without also committing the changes to the repository #
# and deploying the newest version to the roddy plugin directory.        #
##########################################################################

# Variable and Mapping Definition
SCRIPT=$0

roddy_plugin_repo="https://eilslabs-phabricator.dkfz.de/source"
roddy_plugin_repo_github="https://github.com/DKFZ-ODCF"

declare -A pluginRepo

pluginRepo["COWorkflows"]=$roddy_plugin_repo

pluginRepo["COWorkflowsBasePlugin"]=$roddy_plugin_repo_github
pluginRepo["AlignmentAndQCWorkflows"]=$roddy_plugin_repo_github
pluginRepo["RNAseqWorkflow"]=$roddy_plugin_repo_github
pluginRepo["ACEseqWorkflow"]=$roddy_plugin_repo_github
pluginRepo["IndelCallingWorkflow"]=$roddy_plugin_repo_github
pluginRepo["SNVCallingWorkflow"]=$roddy_plugin_repo_github
pluginRepo["SophiaWorkflow"]=$roddy_plugin_repo_github


# Utility Functions
function join_array () {
    printf "%s\n" "${@/#/  - }";
    shift;
}

ERROR="ERROR"; WARNING="WARNING"; INFO="INFO"
function log () {
    type=$1
    shift
    for m in "$@"
    do
        printf "[%-7s]: %s\n" "$type" "$m"
    done
}

function get_usage () {
    echo "Usage: $SCRIPT PLUGIN_NAME TAG [-f|--force] [-h|--help]"
}

function get_available_plugins () {
    echo "$(join_array "${!pluginRepo[@]}")"
}

function display_help () {
    usage=$(get_usage)
    available_plugins=$(get_available_plugins)
    cat <<-EOM
	${usage}

	Clones a plugin repository into a directory named after the plugin and version.

	It only checks out the specific commit of the tag, without history, without other branches.
	The name of the directory it clones to is determined as follows: <plugin_name>_<tag>

	    -f|--force  overrides the pattern matching on the tag name
	    -h|--help   displays this help

	The following plugin names are available:
	${available_plugins}
	EOM
}


# Parameter Handling
while [[ $# -gt 0 ]]
do
    param="$1"
    case $param in
        -h|--help)
        HELP="TRUE"
        shift
        ;;
        -f|--force)
        FORCE="TRUE"
        shift
        ;;
        *)
        POSITIONAL+=("$1")
        shift
        ;;
    esac
done

PLUGIN=${POSITIONAL[0]}
TAG=${POSITIONAL[1]}

## help mode
if [[ $HELP ]]
then
    display_help
    exit
fi

## plugin and tag given
if [ -z "$PLUGIN" ] || [ -z "$TAG" ]
then
    log "$ERROR" "Expected parameters missing"
    echo -e "\n$(get_usage)"
    exit
fi

## plugin name known
if [ -z ${pluginRepo["$PLUGIN"]+x} ]
then
    log "$ERROR" "Unknown plugin name '$PLUGIN', available are:"
    get_available_plugins
    echo -e "\n$(get_usage)"
    exit
fi

## the tag is in the roddy expected format
tag_pattern="^[0-9]+\.[0-9]+\.[0-9]+(-[0-9]+)?$"
if [[ ! "$TAG" =~ $tag_pattern ]]
then
    log "$WARNING" "Tag '$TAG' does not match the expected format"
    if [[ $FORCE ]]
    then
        log "$INFO" "proceeding with force"
        log "$INFO" "rename the created directory to match the roddy expected format!"
        echo ""
    else
        log "$WARNING" "Expected format: '${tag_pattern}'" "Example: 1.2.3 or 1.2.3-4"
        log "$ERROR" "can not continue, exiting"
        echo -e "\n$(get_usage)"
        exit
    fi
fi


# Execution
PLUGIN_REPO_LINK="${pluginRepo[$PLUGIN]}/${PLUGIN}"
TARGET_DIRECTORY="${PLUGIN}_${TAG}"
echo "Cloning ${PLUGIN} ${TAG} from: ${PLUGIN_REPO_LINK}"

git clone --depth 1 --single-branch --branch "$TAG" "$PLUGIN_REPO_LINK" "$TARGET_DIRECTORY"
