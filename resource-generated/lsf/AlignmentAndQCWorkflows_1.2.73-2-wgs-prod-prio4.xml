<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration name="AlignmentAndQCWorkflows:1.2.73-2-wgs-prod-prio4">

    <configurationvalues>
        <cvalue name="workflowEnvironmentScript" value="workflowEnvironment_tbiLsf" type="string"/>
    </configurationvalues>

    <processingTools>

        <tool name="cleanupScript" value="cleanupScript.sh" basepath="qcPipeline"  overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="0.1" cores="1" nodes="1" walltime="1" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='fastqc' value='checkFastQC.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="1" cores="1" nodes="1" walltime="00:10:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="l" nodes="1" walltime="10" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='alignment' value='bwaAlignSequence.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="4" cores="8" nodes="1" walltime="10" queue="prod-prio4"/>
                <rset size="xl" memory="8" cores="8" nodes="1" walltime="10" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='sampesort' value='bwaSampeSort.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="25" cores="6" nodes="1" walltime="00:10:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="s" memory="38" cores="6" nodes="1" walltime="2" queue="prod-prio4"/>
                <rset size="m" memory="42" cores="6" nodes="1" walltime="4" queue="prod-prio4"/>
                <rset size="l" memory="52" cores="6" nodes="1" walltime="10" queue="prod-prio4"/>
                <rset size="xl" memory="75" cores="6" nodes="1" walltime="12" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='sampesortSlim' value='bwaSampeSortSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="25" cores="6" nodes="1" walltime="00:10:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="s" memory="25" cores="6" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="m" memory="30" cores="6" nodes="1" walltime="2" queue="prod-prio4"/>
                <rset size="l" memory="35" cores="8" nodes="1" walltime="7" queue="prod-prio4"/>
                <rset size="xl" memory="35" cores="6" nodes="1" walltime="12" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='alignAndPair' value='bwaMemSort.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="12" cores="8" nodes="1" walltime="00:10:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="s" memory="12" cores="8" nodes="1" walltime="2" queue="prod-prio4"/>
                <rset size="m" memory="17" cores="8" nodes="1" walltime="6" queue="prod-prio4"/>
                <rset size="l" memory="18" cores="8" nodes="1" walltime="35" queue="prod-prio4"/>
                <rset size="xl" memory="20" cores="8" nodes="1" walltime="50" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='alignAndPairSlim' value='bwaMemSortSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="12" cores="8" nodes="1" walltime="00:15:00" queue="prod-prio4"/>
                <!-- ChIP-Seq -->
                <rset size="s" memory="12" cores="8" nodes="1" walltime="2" queue="prod-prio4"/>
                <!-- exome part lane -->
                <rset size="m" memory="17" cores="8" nodes="1" walltime="6" queue="prod-prio4"/>
                <!-- HiSeq full lane. 24 h would be OK but as soon as there are I/O problems on the node, not even 36 h are sufficient - biobambam sort might also be slower than samtools, it seems to use fewer sorting threads	 -->
                <rset size="l" memory="35" cores="8" nodes="1" walltime="60" queue="prod-prio4"/>
                <!-- X10 lane -->
                <rset size="xl" memory="45" cores="8" nodes="1" walltime="180" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='samtoolsIndex' value='samtoolsIndexBamfile.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="1" cores="1" nodes="1" walltime="5" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='collectBamMetrics' value='picardCollectMetrics.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="3" cores="1" nodes="1" walltime="5" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='samtoolsFlagstat' value='samtoolsFlagstatBamfile.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='insertSizes' value='insertSizeDistribution.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='chromosomeDiff' value='differentiateChromosomes.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="s" memory="1" cores="1" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="l" memory="25" cores="1" nodes="1" walltime="5" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='genomeCoverage' value='genomeCoverage.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="s" memory="0.05" cores="4" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="l" memory="0.05" cores="4" nodes="1" walltime="6" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='readBinsCoverage' value='genomeCoverageReadBins.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="0.05" cores="4" nodes="1" walltime="6" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='coveragePlot' value='genomeCoveragePlots.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="0.05" cores="4" nodes="1" walltime="00:15:00"  queue="prod-prio4"/>
                <!-- Production -->
                <rset size="l" memory="0.05" cores="4" nodes="1" walltime="6" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='coveragePlotSingle' value='genomeCoveragePlots.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="5" cores="4" nodes="1" walltime="0:15:0" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="xl" memory="3" cores="4" nodes="1" walltime="10" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicates' value='mergeAndRemoveDuplicates.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="xs" memory="73" cores="8" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="s" memory="73" cores="8" nodes="1" walltime="5" queue="prod-prio4"/>
                <rset size="m" memory="73" cores="8" nodes="1" walltime="20" queue="prod-prio4"/>
                <rset size="l" memory="73" cores="8" nodes="1" walltime="40" queue="prod-prio4"/>
                <rset size="xl" memory="73" cores="8" nodes="1" walltime="80" queue="prod-prio4"/>
            </resourcesets>
        </tool>
        <tool name='mergeAndRemoveDuplicatesSlimPicard' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="50" cores="8" nodes="1" walltime="00:15:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="xs" memory="50" cores="8" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="s" memory="55" cores="8" nodes="1" walltime="2" queue="prod-prio4"/>
                <rset size="m" memory="62" cores="8" nodes="1" walltime="5" queue="prod-prio4"/>
                <rset size="l" memory="60" cores="8" nodes="1" walltime="15" queue="prod-prio4"/>
                <rset size="xl" memory="100" cores="8" nodes="1" walltime="240" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicatesSlimBioBambam' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="5" cores="3" nodes="1" walltime="00:15:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="xs" memory="5" cores="3" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="s" memory="5" cores="3" nodes="1" walltime="2" queue="prod-prio4"/>
                <rset size="m" memory="5" cores="3" nodes="1" walltime="12" queue="prod-prio4"/>
                <rset size="l" memory="15" cores="3" nodes="1" walltime="120" queue="prod-prio4"/>
                <rset size="xl" memory="15" cores="3" nodes="1" walltime="240" queue="prod-prio4"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicatesSlimSambamba' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="30" cores="6" nodes="1" walltime="00:15:00" queue="prod-prio4"/>
                <!-- Production -->
                <rset size="xs" memory="30" cores="6" nodes="1" walltime="1" queue="prod-prio4"/>
                <rset size="s" memory="35" cores="6" nodes="1" walltime="15" queue="prod-prio4"/>
                <rset size="m" memory="40" cores="6" nodes="1" walltime="20" queue="prod-prio4"/>
                <rset size="l" memory="45" cores="6" nodes="1" walltime="30" queue="prod-prio4"/>
                <rset size="xl" memory="100" cores="6" nodes="1" walltime="120" queue="prod-prio4"/>
            </resourcesets>
        </tool>

    </processingTools>

</configuration>
