<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration name="AlignmentAndQCWorkflows:1.2.73-201-wgbs-prod-prio3"
               imports="AlignmentAndQCWorkflows:1.2.73-201-wgs-prod-prio3">

    <configurationvalues>
        <cvalue name="workflowEnvironmentScript" value="workflowEnvironment_tbiLsf" type="string"/>
    </configurationvalues>

    <processingTools>

        <tool name='alignAndPairSlim' value='bwaMemSortSlimWithReadConversionForBisulfiteData.sh' basepath='bisulfiteWorkflow' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="50" cores="8" nodes="1" walltime="1" queue="prod-prio3"/>
                <!-- ChIP-Seq -->
                <rset size="s" memory="1" cores="8" nodes="1" walltime="2" queue="prod-prio3"/>
                <!-- exome part lane -->
                <rset size="m" memory="17" cores="8" nodes="1" walltime="6" queue="prod-prio3"/>
                <!-- HiSeq full lane. 24 h would be OK but as soon as there are I/O problems on the node, not even 36 h are sufficient - biobambam sort might also be slower than samtools, it seems to use fewer sorting threads	 -->
                <rset size="l" memory="75" cores="8" nodes="1" walltime="60" queue="prod-prio3"/>
                <!-- X10 lane -->
                <rset size="xl" memory="75" cores="8" nodes="1" walltime="240" queue="prod-prio3"/>
            </resourcesets>
        </tool>

        <tool name="methylationCalling" value="methylCtools_methylation_calling.sh" basepath="bisulfiteWorkflow" overrideresourcesets="true">
            <resourcesets>
                <rset size="t" queue="prod-prio3"/>
                <rset size="s" queue="prod-prio3"/>
            </resourcesets>
        </tool>

        <tool name="methylationCallingMeta" value="methylCtools_methylation_calling_meta.sh" basepath="bisulfiteWorkflow" overrideresourcesets="true">
            <resourcesets>
                <rset size="t" memory="8" cores="3" nodes="1" walltime="00:15:00" queue="prod-prio3"/>
                <rset size="s" memory="8" cores="13" nodes="1" walltime="1" queue="prod-prio3"/>
                <rset size="m" memory="8" cores="13" nodes="1" walltime="15" queue="prod-prio3"/>
                <rset size="l" memory="8" cores="13" nodes="1" walltime="40" queue="prod-prio3"/>
                <rset size="xl" memory="20" cores="13" nodes="1" walltime="120" queue="prod-prio3"/>
            </resourcesets>
        </tool>

    </processingTools>

</configuration>
