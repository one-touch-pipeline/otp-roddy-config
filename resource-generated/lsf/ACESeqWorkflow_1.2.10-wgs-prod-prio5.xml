<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration name='ACEseqWorkflow:1.2.10-wgs-prod-prio5' description='1.2.8-3+'>
    <configurationvalues>
        <cvalue name="workflowEnvironmentScript" value="workflowEnvironment_tbiLsf" type="string"
                description="Use workflowEnvironment_conda for a generic Conda environment."/>
    </configurationvalues>
    <processingTools>
        <tool overrideresourcesets="true"  name="estimatePurityPloidy" value="purity_ploidy_estimation_final.R" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="2" cores="2" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="generatePlots" value="pscbs_plots.R" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="50" cores="1" nodes="1" walltime="12" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="cnvSnpGeneration" value="cnv_snvMpileup.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="2" cores="2" nodes="1" walltime="15" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="annotateCnvFiles" value="vcfAnno.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="0.2" cores="2" nodes="1" walltime="4" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="replaceBadControl" value="replaceControl.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="0.2" cores="2" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="mergeAndFilterCnvFiles" value="cnvMergeFilter.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="0.2" cores="2" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="mergeAndFilterCnvFiles_withReplaceBadControl" value="cnvMergeFilter.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="0.2" cores="2" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="mergeAndFilterSnpFiles" value="snvMergeFilter.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="0.2" cores="2" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="getGenotypes" value="estimateGenotypes.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="1" cores="1" nodes="1" walltime="3" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="createUnphasedGenotype" value="createUnphasedFiles.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="0.2" cores="1" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="imputeGenotypes_noMpileup" value="impute2.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="10" cores="1" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="imputeGenotypes_X_noMpileup" value="impute2_X.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="10" cores="1" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="imputeGenotypes" value="impute2.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="10" cores="1" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="imputeGenotypes_X" value="impute2_X.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="5" cores="1" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="addHaplotypesToSnpFile" value="haplotypes.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="0.2" cores="2" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="createControlBafPlots" value="createControlBafPlots.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="5" walltime="1" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="correctGcBias" value="correct_gc_bias.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="2" walltime="1" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="getBreakpoints" value="datatablePSCBSgaps.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="30" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="mergeBreakpointsAndSvCrest" value="mergePSCBSCrest.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="0.2" cores="2" nodes="1" walltime="1" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="getSegmentsAndSnps" value="PSCBSall.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="25" cores="2" nodes="1" walltime="20" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="markHomozygousDeletions" value="homozygDel.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="2" cores="1" nodes="1" walltime="1" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="segmentsToSnpDataHomodel" value="segmentsDataHomoDel.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="1" cores="2" nodes="1" walltime="2" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="clusterAndPruneSegments" value="clusteredPrunedNormal.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="20" cores="1" nodes="1" walltime="120" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="segmentsToSnpDataPruned" value="segmentsPrunedNormal.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="1" cores="2" nodes="1" walltime="2" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="estimatePeaksForPurity" value="purityPloidity.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="12" cores="1" nodes="1" walltime="10" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="estimatePurityAndPloidy" value="purityPloidity_EstimateFinal.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="7" cores="1" nodes="1" walltime="5" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="generateResultsAndPlots" value="plots.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="xl" memory="30" cores="1" nodes="1" walltime="15" queue="prod-prio5"/>
            </resourcesets>
        </tool>
        <tool overrideresourcesets="true"  name="estimateHrdScore" value="estimateHRDScore.sh" basepath="copyNumberEstimationWorkflow">
            <resourcesets>
                <rset size="l" memory="1" cores="1" nodes="1" walltime="2"/>
            </resourcesets>
        </tool>
    </processingTools>
</configuration>
