<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration name="AlignmentAndQCWorkflows:1.2.73-2-chipseq">

    <configurationvalues>
        <cvalue name="workflowEnvironmentScript" value="workflowEnvironment_tbiLsf" type="string"/>
    </configurationvalues>

    <processingTools>

        <tool name="cleanupScript" value="cleanupScript.sh" basepath="qcPipeline"  overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="0.1" cores="1" nodes="1" walltime="1"/>
            </resourcesets>
        </tool>

        <tool name='fastqc' value='checkFastQC.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="1" cores="1" nodes="1" walltime="00:10:00" queue="devel"/>
                <!-- Production -->
                <rset size="l" nodes="1" walltime="10"/>
            </resourcesets>
        </tool>

        <tool name='alignment' value='bwaAlignSequence.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="4" cores="8" nodes="1" walltime="10"/>
                <rset size="xl" memory="8" cores="8" nodes="1" walltime="10"/>
            </resourcesets>
        </tool>

        <tool name='accelerated:alignment' value='bwaAlignSequence.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="m" memory="24" cores="12" nodes="1" walltime="1" queue="fpga-0.7.15"/>
                <rset size="l" memory="24" cores="12" nodes="1" walltime="2" queue="fpga-0.7.15"/>
                <rset size="xl" memory="36" cores="12" nodes="1" walltime="5" queue="fpga-0.7.15"/>
            </resourcesets>
        </tool>

        <tool name='sampesort' value='bwaSampeSort.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="25" cores="6" nodes="1" walltime="00:10:00" queue="devel"/>
                <!-- Production -->
                <rset size="s" memory="38" cores="6" nodes="1" walltime="2"/>
                <rset size="m" memory="42" cores="6" nodes="1" walltime="4"/>
                <rset size="l" memory="52" cores="6" nodes="1" walltime="10"/>
                <rset size="xl" memory="75" cores="6" nodes="1" walltime="12"/>
            </resourcesets>
        </tool>

        <tool name='sampesortSlim' value='bwaSampeSortSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="25" cores="6" nodes="1" walltime="00:10:00" queue="devel"/>
                <!-- Production -->
                <rset size="s" memory="25" cores="6" nodes="1" walltime="1"/>
                <rset size="m" memory="30" cores="6" nodes="1" walltime="2"/>
                <rset size="l" memory="35" cores="8" nodes="1" walltime="7"/>
                <rset size="xl" memory="35" cores="6" nodes="1" walltime="12"/>
            </resourcesets>
        </tool>

        <tool name='alignAndPair' value='bwaMemSort.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="12" cores="8" nodes="1" walltime="00:10:00" queue="devel"/>
                <!-- Production -->
                <rset size="s" memory="12" cores="8" nodes="1" walltime="2"/>
                <rset size="m" memory="17" cores="8" nodes="1" walltime="6"/>
                <rset size="l" memory="18" cores="8" nodes="1" walltime="35"/>
                <rset size="xl" memory="20" cores="8" nodes="1" walltime="50"/>
            </resourcesets>
        </tool>

        <tool name='accelerated:alignAndPair' value='bwaMemSort.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="20" cores="12" nodes="1" queue="fpga-0.7.15"/>
                <!-- Production -->
                <rset size="xs" memory="20" cores="12" nodes="1" walltime="1" queue="fpga-0.7.15"/>
                <rset size="s" memory="20" cores="12" nodes="1" walltime="1" queue="fpga-0.7.15"/>
                <rset size="m" memory="20" cores="12" nodes="1" walltime="5" queue="fpga-0.7.15"/>
                <rset size="l" memory="26" cores="12" nodes="1" walltime="15" queue="fpga-0.7.15"/>
                <rset size="xl" memory="26" cores="12" nodes="1" walltime="30" queue="fpga-0.7.15"/>
            </resourcesets>
        </tool>

        <tool name='alignAndPairSlim' value='bwaMemSortSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="50" cores="8" nodes="1" walltime="00:10:00" queue="devel"/>
                <!-- ChIP-Seq -->
                <rset size="s" memory="12" cores="8" nodes="1" walltime="2"/>
                <!-- exome part lane -->
                <rset size="m" memory="17" cores="8" nodes="1" walltime="6"/>
                <!-- HiSeq full lane. 24 h would be OK but as soon as there are I/O problems on the node, not even 36 h are sufficient - biobambam sort might also be slower than samtools, it seems to use fewer sorting threads	 -->
                <rset size="l" memory="35" cores="8" nodes="1" walltime="60"/>
                <!-- X10 lane -->
                <rset size="xl" memory="50" cores="8" nodes="1" walltime="160"/>
            </resourcesets>
        </tool>

        <tool name='accelerated:alignAndPairSlim' value='bwaMemSortSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="126" cores="12" nodes="1" queue="fpga-0.7.15"/>
                <!-- Production -->
                <rset size="s" memory="126" cores="12" nodes="1" walltime="1" queue="fpga-0.7.15"/>
                <rset size="m" memory="126" cores="12" nodes="1" walltime="5" queue="fpga-0.7.15"/>
                <rset size="l" memory="126" cores="12" nodes="1" walltime="15" queue="fpga-0.7.15"/>
                <rset size="xl" memory="126" cores="12" nodes="1" walltime="30" queue="fpga-0.7.15"/>
            </resourcesets>
        </tool>

        <tool name='samtoolsIndex' value='samtoolsIndexBamfile.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="1" cores="1" nodes="1" walltime="5"/>
            </resourcesets>
        </tool>

        <tool name='collectBamMetrics' value='picardCollectMetrics.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="3" cores="1" nodes="1" walltime="5"/>
            </resourcesets>
        </tool>

        <tool name='samtoolsFlagstat' value='samtoolsFlagstatBamfile.sh' basepath='qcPipeline' overrideresourcesets="true">
            <input type="file" typeof="de.dkfz.b080.co.files.BamFile" scriptparameter='FILENAME' />
            <!--<constraint method="hasIndex" methodonfail="index"/>-->
            <!--</input>-->
            <output type="file" typeof="de.dkfz.b080.co.files.FlagstatsFile" scriptparameter='FILENAME_FLAGSTAT'/>
        </tool>

        <tool name='insertSizes' value='insertSizeDistribution.sh' basepath='qcPipeline' overrideresourcesets="true">

        </tool>

        <tool name='chromosomeDiff' value='differentiateChromosomes.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="s" memory="1" cores="1" nodes="1" walltime="1"/>
                <rset size="l" memory="25" cores="1" nodes="1" walltime="5"/>
            </resourcesets>
        </tool>

        <tool name='genomeCoverage' value='genomeCoverage.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="s" memory="0.05" cores="4" nodes="1" walltime="1"/>
                <rset size="l" memory="0.05" cores="4" nodes="1" walltime="6"/>
            </resourcesets>
        </tool>

        <tool name='readBinsCoverage' value='genomeCoverageReadBins.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="l" memory="0.05" cores="4" nodes="1" walltime="6"/>
            </resourcesets>
        </tool>

        <tool name='coveragePlot' value='genomeCoveragePlots.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="5" cores="4" nodes="1" queue="devel"/>
                <!-- Production -->
                <rset size="l" memory="5" cores="4" nodes="1" walltime="6"/>
            </resourcesets>
        </tool>

        <tool name='coveragePlotSingle' value='genomeCoveragePlots.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="5" cores="4" nodes="1" walltime="00:15:00" queue="devel"/>
                <!-- Production -->
                <rset size="xl" memory="2" cores="4" nodes="1" walltime="6"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicates' value='mergeAndRemoveDuplicates.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <rset size="xs" memory="73" cores="8" nodes="1" walltime="1"/>
                <rset size="s" memory="73" cores="8" nodes="1" walltime="5"/>
                <rset size="m" memory="73" cores="8" nodes="1" walltime="20"/>
                <rset size="l" memory="73" cores="8" nodes="1" walltime="40"/>
                <rset size="xl" memory="73" cores="8" nodes="1" walltime="80"/>
            </resourcesets>
        </tool>
        <tool name='mergeAndRemoveDuplicatesSlimPicard' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="50" cores="8" nodes="1" walltime="00:15:00" queue="devel"/>
                <!-- Production -->
                <rset size="xs" memory="50" cores="8" nodes="1" walltime="1"/>
                <rset size="s" memory="55" cores="8" nodes="1" walltime="2"/>
                <rset size="m" memory="62" cores="8" nodes="1" walltime="5"/>
                <rset size="l" memory="60" cores="8" nodes="1" walltime="15"/>
                <rset size="xl" memory="100" cores="8" nodes="1" walltime="240"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicatesSlimBioBambam' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="5" cores="3" nodes="1" walltime="00:15:00" queue="devel"/>
                <!-- Production -->
                <rset size="xs" memory="5" cores="3" nodes="1" walltime="1"/>
                <rset size="s" memory="5" cores="3" nodes="1" walltime="2"/>
                <rset size="m" memory="5" cores="3" nodes="1" walltime="12"/>
                <rset size="l" memory="15" cores="3" nodes="1" walltime="120"/>
                <rset size="xl" memory="15" cores="3" nodes="1" walltime="240"/>
            </resourcesets>
        </tool>

        <tool name='mergeAndRemoveDuplicatesSlimSambamba' value='mergeAndMarkOrRemoveDuplicatesSlim.sh' basepath='qcPipeline' overrideresourcesets="true">
            <resourcesets>
                <!-- Test -->
                <rset size="t" memory="30" cores="6" nodes="1" walltime="00:15:00" queue="devel"/>
                <!-- Production -->
                <rset size="xs" memory="30" cores="6" nodes="1" walltime="1"/>
                <rset size="s" memory="35" cores="6" nodes="1" walltime="15"/>
                <rset size="m" memory="40" cores="6" nodes="1" walltime="20"/>
                <rset size="l" memory="45" cores="6" nodes="1" walltime="30"/>
                <rset size="xl" memory="80" cores="6" nodes="1" walltime="80"/>
            </resourcesets>
        </tool>

    </processingTools>

</configuration>
