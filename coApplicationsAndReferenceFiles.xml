<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->
<configuration name='coAppAndRef'
               description='Configuration which contains application names and reference files (like i.e. indices).'>
    <configurationvalues>
        <cvalue name="assembliesBaseDirectory" value="${sharedFilesBaseDirectory}/assemblies"
                type="path"/>
        <cvalue name="assembliesHG191000GenomesDirectory"
                value="${assembliesBaseDirectory}/hg19_GRCh37_1000genomes" type="path"/>

        <cvalue name="bwaIndexBaseDirectory_human"
                value="${assembliesHG191000GenomesDirectory}/indexes/bwa" type="path"/>
        <cvalue name="bwaIndexBaseDirectory_methylCtools_human"
                value="${assembliesHG191000GenomesDirectory}/indexes/methylCtools" type="path"/>

        <cvalue name="referenceGenomeBaseDirectory_human"
                value="${assembliesHG191000GenomesDirectory}/sequence" type="path"/>
        <cvalue name="chromosomeSizesBaseDirectory_human"
                value="${assembliesHG191000GenomesDirectory}/stats" type="path"/>
        <cvalue name="targetRegionsBaseDirectory_human"
                value="${assembliesHG191000GenomesDirectory}/targetRegions" type="path"/>

        <cvalue name="hg19DatabasesDirectory"
                value="${assembliesHG191000GenomesDirectory}/databases" type="path"/>
        <cvalue name='hg19DatabaseUCSCDirectory' value='${hg19DatabasesDirectory}/UCSC'
                type="path"/>
        <cvalue name='hg19DatabaseDBSNPDirectory' value='${hg19DatabasesDirectory}/dbSNP'
                type="path"/>
        <cvalue name='hg19Database1000GenomesDirectory'
                value='${hg19DatabasesDirectory}/1000genomes' type="path"/>
        <cvalue name='hg19DatabaseIMPUTEDirectory'
                value='${hg19Database1000GenomesDirectory}/IMPUTE' type="path"/>
        <cvalue name='hg19DatabaseENCODEDirectory' value='${hg19DatabasesDirectory}/ENCODE'
                type="path"/>

        <cvalue name="bwaIndexBaseDirectory_mm10"
                value="${assembliesBaseDirectory}/mm10/indexes/bwa" type="path"/>
        <cvalue name="bwaIndexBaseDirectory_methylCtools_mm10"
                value="${assembliesBaseDirectory}/mm10/indexes/methylCtools" type="path"/>
        <cvalue name="chromosomeSizesBaseDirectory_mm10"
                value="${assembliesBaseDirectory}/mm10/stats" type="path"/>
        <cvalue name="targetRegionsBaseDirectory_mm10"
                value="${assembliesBaseDirectory}/mm10/targetRegions" type="path"/>


        <cvalue name="meth_calls_converter_moabs" value="" type="string"/>

        <!--1KG  - 1000 genome
            hg19 - human genome v19
            mm   - mouse genome v10 -->
        <cvalue name="indexPrefix_bwa05_1KGRef"
                value="${bwaIndexBaseDirectory_human}/bwa05_1KGRef/hs37d5.fa" type="path"/>
        <cvalue name="indexPrefix_bwa05_hg19_chr"
                value="${bwaIndexBaseDirectory_human}/bwa05_hg19_chr/hg19bwaidx" type="path"/>
        <cvalue name="indexPrefix_bwa06_1KGRef"
                value="${bwaIndexBaseDirectory_human}/bwa06_1KGRef/hs37d5.fa" type="path"/>
        <cvalue name="indexPrefix_bwa06_hg19_chr"
                value="${bwaIndexBaseDirectory_human}/bwa06_hg19_chr/hg19_1-22_X_Y_M.fasta"
                type="path"/>
        <cvalue name="indexPrefix_bwa06_mm10_GRC"
                value="${bwaIndexBaseDirectory_mm10}/bwa06/bwa06_GRCm38mm10/GRCm38mm10.fa"
                type="path"/>
        <cvalue name="indexPrefix_bwa06_mm10"
                value="${bwaIndexBaseDirectory_mm10}/bwa06/bwa06_mm10_UCSC/mm10_1-19_X_Y_M.fa"
                type="path"/>
        <cvalue name="indexPrefix_bwa06_methylCtools_mm10_GRC"
                value="${bwaIndexBaseDirectory_methylCtools_mm10}/methylCtools_GRCm38mm10/GRCm38mm10_PhiX_Lambda.conv.fa"
                type="path"/>
        <cvalue name="indexPrefix_bwa06_methylCtools_mm10_UCSC"
                value="${bwaIndexBaseDirectory_methylCtools_mm10}/methylCtools_mm10_UCSC/mm10_PhiX_Lambda.conv.fa"
                type="path"/>
        <cvalue name="indexPrefix_bwa06_methylCtools_1KGRef"
                value="${bwaIndexBaseDirectory_methylCtools_human}/methylCtools_1KGRef/hs37d5_PhiX_Lambda.conv.fa"
                type="path"/>

        <cvalue name="ch_pos_index_methylCtools_1KGRef"
                value="${bwaIndexBaseDirectory_methylCtools_human}/methylCtools_1KGRef/hs37d5_PhiX_Lambda.CG_CH.pos.gz"
                type="path"/>
        <cvalue name="ch_pos_index_methylCtools_mm10GRC"
                value="${bwaIndexBaseDirectory_methylCtools_mm10}/methylCtools_GRCm38mm10/GRCm38mm10_PhiX_Lambda.pos.gz"
                type="path"/>
        <cvalue name="ch_pos_index_methylCtools_mm10_UCSC"
                value="${bwaIndexBaseDirectory_methylCtools_mm10}/methylCtools_mm10_UCSC/mm10_PhiX_Lambda.pos.gz"
                type="path"/>

        <!-- TODO There should be a ruleset for optional rules. Also reference genomes should be checked. -->
        <cvalue name="referenceGenome_1KGRef"
                value="${referenceGenomeBaseDirectory_human}/1KGRef/hs37d5.fa" type="path"/>
        <cvalue name="referenceGenome_hg19_chr"
                value="${referenceGenomeBaseDirectory_human}/hg19_chr/hg19_1-22_X_Y_M.fa"
                type="path"/>

        <!-- Chromosome sizes file for hg 19 -->
        <cvalue name='chromosomeSizesFile_hg19'
                value='${chromosomeSizesBaseDirectory_human}/hg19_1-22_X_Y_M.fa.chrLenOnlyACGT.tab'
                type="path"/>

        <!-- Chromosome sizes file for 1000 genome -->
        <cvalue name='chromosomeSizesFile_hs37'
                value='${chromosomeSizesBaseDirectory_human}/hs37d5.fa.chrLenOnlyACGT_realChromosomes.tab'
                type="path"/>
        <cvalue name='chromosomeSizesFile_mm10_GRC'
                value='${chromosomeSizesBaseDirectory_mm10}/GRCm38mm10.fa.chrLenOnlyACGT_realChromosomes.tab'
                type="path"/>
        <cvalue name='chromosomeSizesFile_mm10'
                value='${chromosomeSizesBaseDirectory_mm10}/mm10_1-19_X_Y_M.fa.chrLenOnlyACGT_realChromosomes.tab'
                type="path"/>
        <cvalue name='chromosomeSizesFile_hs37_bisulfite'
                value='${chromosomeSizesBaseDirectory_human}/hs37d5_PhiX_Lambda.fa.chrLenOnlyACGT.tab'
                type="path"/>
        <cvalue name='chromosomeSizesFile_mm10_GRC_bisulfite'
                value='${chromosomeSizesBaseDirectory_mm10}/GRCm38mm10_PhiX_Lambda.fa.chrLenOnlyACGT.tab'
                type="path"/>
        <cvalue name='chromosomeSizesFile_mm10_UCSC_bisulfite'
                value='${chromosomeSizesBaseDirectory_mm10}/mm10_PhiX_Lambda.fa.chrLenOnlyACGT.tab'
                type="path"/>
        <cvalue name='chromosomeLengthFile_hg19'
                value='${chromosomeSizesBaseDirectory_human}/hg19_chrTotalLength.tsv' type="path"/>

        <cvalue name='targetRegions_Agilent4withoutUTRs_chr'
                value='${targetRegionsBaseDirectory_human}/Agilent4withoutUTRs_chr.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent4withoutUTRs_plain'
                value='${targetRegionsBaseDirectory_human}/Agilent4withoutUTRs_plain.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent4withUTRs_plain'
                value='${targetRegionsBaseDirectory_human}/Agilent4withUTRs_plain.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent5withoutUTRs_chr'
                value='${targetRegionsBaseDirectory_human}/Agilent5withoutUTRs_chr.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent5withoutUTRs_plain'
                value='${targetRegionsBaseDirectory_human}/Agilent5withoutUTRs_plain.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent5withUTRs_chr'
                value='${targetRegionsBaseDirectory_human}/Agilent5withUTRs_chr.bed.gz'
                type="path"/>
        <cvalue name='targetRegions_Agilent5withUTRs_plain'
                value='${targetRegionsBaseDirectory_human}/Agilent5withUTRs_plain.bed.gz'
                type="path"/>

        <cvalue name="dbSNP_FILE"
                value="${hg19DatabasesDirectory}/dbSNP/dbSNP_135/00-All.SNV.vcf.gz" type="path"/>

        <cvalue name='SNP_REFERENCE'
                value='${assembliesHG191000GenomesDirectory}/sequence/hg19_chr/hg19_1-22_X_Y_M.fa'
                type="path"/>
        <cvalue name='SNP_REFERENCE_ANNOTATIONS'
                value='${assembliesHG191000GenomesDirectory}/Affymetrics/Affy5/chr/#CHROM#_AFFY.vcf'
                type="path"/>

    </configurationvalues>
</configuration>
