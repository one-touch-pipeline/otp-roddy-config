<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration configurationType='project' name='coBaseProject'
               description='Base configuration for co projects.' imports='cofilenames,coAppAndRef'>
    <configurationvalues>
        <cvalue name="alignmentThreads" value="12" type="string"/>

        <cvalue name='outputUMask' value='007' type='string'/>
        <cvalue name='outputFileGroup' value='false'/>
        <cvalue name='outputAccessRights' value='u+rw,g+rw,o-rwx'/>
        <cvalue name='outputAccessRightsForDirectories' value='u+rwx,g+rwx,o-rwx'/>

        <cvalue name='possibleControlSampleNamePrefixes'
                value='( blood BLOOD normal control CONTROL buffy_coat GERMLINE )'
                type='bashArray'/>
        <cvalue name='possibleTumorSampleNamePrefixes'
                value='( tumor TUMOR metastasis xenograft disease DISEASE relapse RELAPSE autopsy AUTOPSY metastasis METASTASIS )'
                type='bashArray'/>

        <cvalue name="useCentralAnalysisArchive" value="true"/>
        <cvalue name="enableJobProfiling" value="false"/>

        <cvalue name="JOB_PROFILER_BINARY" value="strace.sh"/>

        <cvalue name='INDEX_PREFIX' value='${indexPrefix_bwa05_hg19_chr}' type="path"/>
        <cvalue name='BWA_ALIGNMENT_OPTIONS' value='"-q 20"'/>
        <cvalue name='BWA_SAMPESORT_OPTIONS' value='"-a 1000"'/>
        <cvalue name="SAMPESORT_MEMSIZE" value="2000000000" type="integer"/>

        <cvalue name="BWA_MEM_OPTIONS" value='" -T 0 "' type="string"/>
        <cvalue name="BWA_MEM_CONVEY_ADDITIONAL_OPTIONS"
                value='"--bb_cny_timeout=5000000000 --bb_profile=1 -t 8"' type="string"/>

        <cvalue name="mergeAndRemoveDuplicates_optionMarkDuplicates"
                value='" REMOVE_DUPLICATES=FALSE"' type="string"/>
        <cvalue name="mergeAndRemoveDuplicates_removeDuplicates" value='" REMOVE_DUPLICATES=TRUE"'
                type="string"/>
        <cvalue name="mergeAndRemoveDuplicates_argumentList"
                value='${mergeAndRemoveDuplicates_optionMarkDuplicates}' type="string"/>

        <cvalue name='LIB_ADD' value='addToOldLib'/>

        <cvalue name='QUAL' value='phred' description="Default quality is phred, can be illumina."/>

        <cvalue name='SNP_MINCOVERAGE' value='16'/>
        <cvalue name='SNP_MAXCOVERAGE' value='300'/>
        <!-- TODO make validation rule for chrom sizes file and chr_prefix -->
        <cvalue name='CHROM_SIZES_FILE' value='${chromosomeSizesFile_hg19}' type="path"/>

        <cvalue name='CLIP_INDEX'
                value='${DIR_EXECUTION}/analysisTools/qcPipelineTools/trimmomatic/adapters/TruSeq3-PE.fa'
                type="path"/>
        <cvalue name='ADAPTOR_TRIMMING_OPTIONS_0' value='"PE -threads 12 -phred33"'/>
        <cvalue name='ADAPTOR_TRIMMING_OPTIONS_1'
                value='"ILLUMINACLIP:${CLIP_INDEX}:2:30:10:8:true SLIDINGWINDOW:4:15 MINLEN:36"'/>
    </configurationvalues>
</configuration>
