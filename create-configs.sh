
#
# Copyright 2011-2024 The OTP authors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
#

###
# Script to create multiple sets of valid queue-specific configs based given queues
#
# This script is used to generate queue specific configs for all queues provided
# in the file defined by QUEUE_NAMES_FILE. The configs are based on the templates
# provided in TEMPLATE_ROOT and outputs the configs into CONFIG_OUTPUT.
#
# The generated configs are not supposed to be changed. All changes should be
# done to the templates, then regenerate the configs, double check the updated
# configs and then check them into the repository.
#
# It first removes all existing configs from CONFIG_OUTPUT and then generates
# the new configs. This is done to ensure a strict before and after diff.
#
# Paths in the config are relative to this scripts location.
##

###
# Config

# file defining the queue names
QUEUE_NAMES_FILE="cluster-queue-names.txt"

# path to the template files
TEMPLATE_ROOT="resource-templates/lsf"

# output directory for valid configs
CONFIG_OUTPUT="resource-generated/lsf"


###
# Execution

set -e

ROOT=$(dirname "$(realpath $0)")
ABSOLUTE_TEMPLATES="${ROOT}/${TEMPLATE_ROOT}"
ABSOLUTE_OUTPUT="${ROOT}/${CONFIG_OUTPUT}"

queue_list="$(grep -vE "^#" $QUEUE_NAMES_FILE)"

# security checks
# queue names should not contains any `_` to be secure with Roddy
for queue in $queue_list
do
    if [[ $queue == *"_"* ]]; then
        echo -e "Queue name '${queue}' contains illegal character '_'\nexiting"
        exit
    fi
done

# cleaning
echo -e "cleaning old configs from:\n$ABSOLUTE_OUTPUT\n"
rm -r "$ABSOLUTE_OUTPUT"
mkdir -p "$ABSOLUTE_OUTPUT"

# generation
echo -e "generating new configs into:\n$ABSOLUTE_OUTPUT\n"
echo -e "taking templates from:\n$ABSOLUTE_TEMPLATES\n"

for queue in $queue_list
do
    echo "Queue: '$queue'"
    for file in ${ABSOLUTE_TEMPLATES}/*
    do
        new_filename="`basename "$file" | sed "s/queue/${queue}/g"`"
	cat "$file" | sed "s/QUEUE_NAME/${queue}/g" > "${ABSOLUTE_OUTPUT}/${new_filename}"
    done
done
