<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration configurationType='project' name='otpPanCanAlignmentWorkflow-1.4' imports="otp-1.1"
               description='All parameters specific to the PanCan Alignment Workflow. The top part a encompasses parameters that affect for the results and are set to defaults that reflect the *official* PCAWF.'>
    <configurationvalues>

        <!-- BWA parameters -->
        <cvalue name='useAcceleratedHardware' value='false' type="boolean"
                description="Use the Convey or not. Default for official PCAWF: false"/>

        <cvalue name="BWA_MEM_OPTIONS" value='" -T 0 "' type="string"
                description="Defaults to ' -T 0 '"/>

        <!-- Duplication marking: Biobambam versus Picard -->
        <cvalue name='useBioBamBamMarkDuplicates' value='true'
                description='Use biobambam (true; PCAWF) or Picard (false).'/>

        <!-- Coverage calculation: coverageQc D program vs. python script -->
        <cvalue name='useDefaultCoverageQCTool' value='false' type="boolean"
                description="This variable is used in genomeCoverageReadBins.sh and there defaults to false. false = D coverage tool. true = python coverage tool (UNTESTED QCJSON.PL!)."/>
        <!-- <cvalue name='BASE_QUALITY_CUTOFF' value='0' type="integer" -->
        <!-- 	      description="Reads are not considered for the coverage calculation if their average base quality is &lt; BASE_QUALITY_CUTOFF. WARNING: This parameter is shared for WGS and WES (and maybe others in the plugin)."/> -->

        <!-- Used by the script flags_isizes_PEaberrations.pl -->
        <cvalue name="INSERT_SIZE_LIMIT" value="1000" type="integer"
                description="Make the value a lot larger for mate pair data. Default: 1000"/>

        <!-- WES-only -->
        <cvalue name='SAMTOOLS_VIEW_OPTIONS' value='" -bu -q 1 -F 1024"'
                description="IMPORTANT: -q 1 uniquely mapped reads. -bu output uncompressed BAM for pipe to coverageBed."/>
        <!-- End of WES-only -->

        <!-- Adapter clipping can be but hardly *is* used. -->
        <cvalue name='useAdaptorTrimming' value='false'
                description='Use trimmomatic to trim off adapters.'/>
        <cvalue name='CLIP_INDEX'
                value='$DIR_EXECUTION/analysisTools/qcPipeline/trimmomatic/adapters/TruSeq3-PE.fa'
                type="path"/>
        <cvalue name='ADAPTOR_TRIMMING_OPTIONS_0' value='"PE -threads 12 -phred33"'/>
        <cvalue name='ADAPTOR_TRIMMING_OPTIONS_1'
                value='"ILLUMINACLIP:$CLIP_INDEX:2:30:10:8:true SLIDINGWINDOW:4:15 MINLEN:36"'/>

        <!-- FastQC -->
        <cvalue name='runFastQC' value='false' type='boolean'
                description="Run FastQc or not."/>

        <!-- These are not technical but essential to the PCAWF. -->
        <cvalue name='useCombinedAlignAndSampe' value='true' type="boolean"/>
        <cvalue name='runSlimWorkflow' value='true' type="boolean"/>

        <!-- Software versions -->
        <cvalue name="BWA_VERSION" value="0.7.8" type="string"/>
        <cvalue name="PICARD_VERSION" value="1.125" type="string"/>
        <cvalue name='BEDTOOLS_VERSION' value='2.16.2' type='string'/>
        <cvalue name="FASTQC_VERSION" value="0.10.1" type="string"/>
        <cvalue name="SAMTOOLS_VERSION" value="0.1.19" type="string"/>
        <cvalue name="SAMBAMBA_VERSION" value="0.4.6" type="string"/>
        <cvalue name="SAMBAMBA_MARKDUP_VERSION" value="0.4.6" type="string"/>
        <cvalue name="BIOBAMBAM_VERSION" value="0.0.148" type="string"/>
        <cvalue name="HTSLIB_VERSION" value="0.2.5" type="string"/>

        <!-- Technical parameters of the PCAWF pipeline without impact on the outcome. E.g. tuning parameters. -->
        <cvalue name="BWA_MEM_THREADS" value="8" type="integer" description="Threads for bwa mem."/>

        <!-- Purely technical parameters. Don't change anything after this line! -->
        <cvalue name='mergedBamSuffixList' value='${mergedBamSuffix_markDuplicatesShort}'
                type="string"
                description="A list of all known suffixes for merged bam files. I.e. merged.dupmark.bam, merged.mdup.bam..."/>

    </configurationvalues>

    <!-- Filename definitions. Don't change these, unless you know what you do. -->
    <filenames package='de.dkfz.b080.co.files' filestagesbase='de.dkfz.b080.co.files.COFileStage'>
        <!-- Filenames are always stored in the pid's output folder -->
        <!-- Different variables can be used:
         - ${sourcefile}, use the name and the path of the file from which the new name is derived
         - ${sourcefileAtomic}, use the atomic name of which the file is derived
         - ${sourcefileAtomicPrefix,delimiter=".."}, use the atomic name's prefix (without file-ending like .txt/.paired.bam...
         of which the file is derived, set the delimiter option to define the delimiter default is "_"
         the delimiter has to be placed inside "" as this is used to find the delimiter!
         - ${sourcepath}, use the path in which the source file is stored
         - ${outputbasepath}, use the output path of the pid
         - ${[nameofdir]OutputDirectory}

             NOTICE: If you use options for a variable your are NOT allowed to use ","! It is used to recognize options.

             - ${pid}
         - ${sample}
         - ${run}
         - ${lane}
         - ${laneindex}
         - You can put in configuration values to do this use:
         ${cvalue,name=[name of the value],default=".."} where default is optional.
         - ${fileStageID} use the id String of the file's stage to build up the name.
        -->
        <!-- A filename can be derived from another file, use derivedFrom='shortClassName/longClassName'
         A filename can also be specified for a level, use fileStage='PID/SAMPLE/RUN/LANE/INDEXEDLANE', refer to BaseFile.FileStage
         A filename can be specified for all levels, the name is then build up with the ${fileStageID} value
         A filename can be created using the file's called method's name
         A filename can be created using the used tool's name
        -->

        <!-- single lane BAM files with BWA-MEM/slim workflow -->
        <filename class='BamIndexFile' derivedFrom='BamFile' pattern='${sourcefile}.bai'/>
        <!-- <filename class='FastqcFile' onMethod='LaneFile.calcFastqc' -->
        <!-- 	  pattern="${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${fastx_qcOutputDirectory}/${fileStageID}_sequence_fastqc.zip"/> -->

        <filename class='BamFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/${sample}_${run}_${lane}_${cvalue,name="pairedBamSuffix"}'/>
        <filename class='FlagstatsFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${flagstatsOutputDirectory}/${sourcefileAtomic}_flagstats.txt'/>
        <filename class='TextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag="extendedFlagstats"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${flagstatsOutputDirectory}/${sourcefileAtomic}_extendedFlagstats.txt'/>
        <filename class='BamMetricsFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${sourcefileAtomic}.dupmark_metrics.txt'/>
        <filename class='ChromosomeDiffValueFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png_qcValues.txt'/>
        <filename class='ChromosomeDiffTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.txt'/>
        <filename class='ChromosomeDiffPlotFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png'/>
        <filename class='InsertSizesValueFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png_qcValues.txt'/>
        <filename class='TextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip_densityPlot.png'/>
        <filename class='InsertSizesTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsizes.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png'/>
        <filename class='QCSummaryFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${sourcefileAtomicPrefix,delimiter="_"}_${sourcefileProperty,type}_wroteQcSummary.txt'/>
        <filename class='CoverageTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag='genomeCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.DepthOfCoverage_Genome.txt'/>
        <filename class='CoverageTextFile' onMethod='LaneFileGroup.alignAndPairSlim'
                  selectiontag='readBinsCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows.txt'/>
        <filename class="TextFile" onMethod='LaneFileGroup.alignAndPairSlim' selectiontag="qcJson"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${run}_${lane}/qualitycontrol.json'/>
        <!--<filename class='TextFile' onMethod="LaneFileGroup.alignAndPairSlim" selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip.txt'/>-->
        <!--<filename class='InsertSizesPlotFile' onMethod="LaneFileGroup.alignAndPairSlim"
                      selectiontag="dipStatistics" pattern='${outputAnalysisBaseDirectory}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip_densityPlot.png'/>-->


        <!-- merged BAM file -->
        <filename class='BamFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/${sample}_${pid}_${cvalue,name="defaultMergedBamSuffix"}'/>
        <filename class='FlagstatsFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_flagstats.txt'/>
        <filename class='TextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="extendedFlagstats"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_extendedFlagstats.txt'/>
        <filename class='BamMetricsFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${sourcefileAtomic}.dupmark_metrics.txt'/>
        <filename class='ChromosomeDiffValueFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png_qcValues.txt'/>
        <filename class='ChromosomeDiffTextFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.txt'/>
        <filename class='ChromosomeDiffPlotFile'
                  onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png'/>
        <filename class='InsertSizesValueFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png_qcValues.txt'/>
        <filename class='TextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip_densityPlot.png'/>
        <filename class='InsertSizesTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsizes.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png'/>
        <filename class='QCSummaryFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${sourcefileAtomicPrefix,delimiter="_"}_${sourcefileProperty,type}_wroteQcSummary.txt'/>
        <filename class='CoverageTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag='genomeCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}.DepthOfCoverage_Genome.txt'/>
        <filename class='CoverageTextFile' onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag='readBinsCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows.txt'/>
        <filename class="TextFile" onMethod='BamFileGroup.mergeAndRemoveDuplicatesSlim'
                  selectiontag="qcJson"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/qualitycontrol.json'/>
        <!--<filename class='TextFile' onMethod="BamFileGroup.mergeAndRemoveDuplicatesSlim" selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip.txt'/>-->
        <!--<filename class='InsertSizesPlotFile' onMethod="BamFileGroup.mergeAndRemoveDuplicatesSlim"
                      selectiontag="dipStatistics" pattern='${outputAnalysisBaseDirectory}/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip_densityPlot.png'/>-->

        <!-- coverage files -->
        <filename class='GenomeCoveragePlotFile' derivedFrom='CoverageTextFile[2]'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${pid}_${sample[0]}_vs_${sample[1]}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows_coveragePlot.png'/>
        <filename class='GenomeCoveragePlotFile' derivedFrom='CoverageTextFile'
                  selectiontag="singlePlot"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${pid}_${sample}_readCoverage_${cvalue,name="WINDOW_SIZE",default="1"}kb_windows_coveragePlot.png'/>

        <!-- WES output files -->

        <!-- TODO: This rule is necessary for the derived statistics files, even if the file itself is not produced. -->
        <filename class='BamFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  pattern='${outputAnalysisBaseDirectory}/${sample}_${pid}_${cvalue,name="TARGET_BAM_EXTENSION",default="targetExtract.rmdup.bam"}'/>

        <!-- output FILENAME_INDEX covered by general rule above (though not produced) -->

        <!-- flagstats -->
        <filename class='FlagstatsFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_flagstats.txt'/>
        <filename class='TextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag="extendedFlagstats"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${flagstatsOutputDirectory}/${sourcefileAtomic}_extendedFlagstats.txt'/>

        <!-- ChromDiff statistics -->
        <filename class='ChromosomeDiffValueFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png_qcValues.txt'/>
        <filename class='ChromosomeDiffTextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.txt'/>
        <filename class='ChromosomeDiffPlotFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${structuralVariationOutputDirectory}/${sourcefileAtomic}_DiffChroms.png'/>

        <!-- Insert size statistics -->
        <filename class='InsertSizesValueFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag='targetExtract'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_insertsize_plot.png_qcValues.txt'/>
        <filename class='InsertSizesTextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag='targetExtract'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_insertsizes.txt'/>
        <filename class='InsertSizesPlotFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag='targetExtract'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_insertsize_plot.png'/>

        <filename class='TextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag="dipStatistics"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${insertSizesOutputDirectory}/${sourcefileAtomic}_HartigansDip.txt'/>

        <!-- Coverage statistics -->
        <filename class='CoverageTextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag='genomeCoverage'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomic}.DepthOfCoverage_Target.txt'/>

        <!-- the only file that is specific to the targetExtractCoverageSlim job -->
        <filename class='CoverageTextFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag="targetsWithCoverage"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${coverageOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_TargetsWithCov.txt'/>

        <filename class='QCSummaryFile' onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag='targetExtract'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/${sourcefileAtomicPrefix,delimiter="_"}_wroteQcSummary.txt'/>

        <filename class="TextFile" onMethod='BamFile.extractTargetsCalculateCoverage'
                  selectiontag="qcJson"
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/merged/qualitycontrol_targetExtract.json'/>

    </filenames>

</configuration>
