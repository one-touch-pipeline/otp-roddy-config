<!--
  ~ Copyright 2011-2024 The OTP authors
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  ~
  -->

<configuration configurationType='project' name='otpSophia-1.1' imports="otp-1.2">
    <configurationvalues>
        <cvalue name="disableAutoBAMHeaderAnalysis" value="true" type="boolean"
                description="For xenograft this should be set to 'true'."/>
        <cvalue name="sophiaOutputDirectory" value="." type="path"/>
    </configurationvalues>
    <filenames package='de.dkfz.b080.co.files' filestagesbase='de.dkfz.b080.co.files.COFileStage'>
        <filename class='InsertSizesValueFile' derivedFrom='BasicBamFile'
                  pattern='${outputAnalysisBaseDirectory}/qualitycontrol/${sample}/merged/${insertSizesOutputDirectory}/${sourcefileAtomicPrefix,delimiter="_"}_insertsize_plot.png_qcValues.txt'/>

        <filename class='SophiaOutputBedpe' derivedFrom="BasicBamFile"
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/${sample}_${pid}_bps.tsv.gz'/>

        <filename class='AnnotationFilteredGermlineBedpeFileNoControl'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-only_filtered_germlineStrict.tsv'/>
        <filename class='AnnotationFilteredBedpeFileNoControl' derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-only_filtered.tsv'/>
        <filename class='AnnotationFilteredSomaticBedpeFileNoControl'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-only_filtered_somatic.tsv'/>
        <filename class='AnnotationFilteredSomaticBedpeFileNoControlACEseq'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_filtered_somatic_minEventScore3.tsv'/>
        <filename class='AnnotationFilteredQJsonNoControl' derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-only.qualitycontrol.json'/>


        <filename class='AnnotationFilteredGermlineBedpeFileTmVsCntrl'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-${controlSample}_filtered_germlineStrict.tsv'/>
        <filename class='AnnotationFilteredBedpeFileTmVsCntrl' derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-${controlSample}_filtered.tsv'/>
        <filename class='AnnotationFilteredSomaticBedpeFileTmVsCntrl'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_${tumorSample}-${controlSample}_filtered_somatic.tsv'/>
        <filename class='AnnotationFilteredSomaticBedpeFileTmVsCntrlACEseq'
                  derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/svs_${pid}_filtered_somatic_minEventScore3.tsv'/>
        <filename class='AnnotationFilteredQJsonTmVsCntrl' derivedFrom='SophiaOutputBedpe'
                  pattern='${outputAnalysisBaseDirectory}/${sophiaOutputDirectory}/qualitycontrol.json'/>

    </filenames>
</configuration>
